"""Test templatags."""
import datetime
from unittest import mock

from django.conf import settings
from django.http.request import QueryDict
from django.test.utils import override_settings
from django.utils import timezone

from datawarehouse import models
from datawarehouse.templatetags import artifacts
from datawarehouse.templatetags import avatar
from datawarehouse.templatetags import queryset_utils
from datawarehouse.templatetags import serialize
from datawarehouse.templatetags import strutils
from datawarehouse.templatetags import url_replace
from tests import utils


class TestQuerysetUtils(utils.TestCase):
    """Test queryset_utils templatetags."""

    def test_trim_queryset(self):
        """Test trim_queryset templatetag."""
        models.GitTree.objects.bulk_create([
            models.GitTree(name=name)
            for name in range(15)
        ])

        queryset = models.GitTree.objects.all().order_by('id')

        # Test default length: 10
        trimmed = queryset_utils.trim_queryset(queryset)
        self.assertEqual(10, len(trimmed))
        self.assertEqual(5, trimmed.trimmed_elements)
        self.assertListEqual(
            [item.name for item in trimmed],
            [str(item) for item in range(10)]
        )

        # Test length as parameter
        trimmed = queryset_utils.trim_queryset(queryset, trim_at=5)
        self.assertEqual(5, len(trimmed))
        self.assertEqual(10, trimmed.trimmed_elements)
        self.assertListEqual(
            [item.name for item in trimmed],
            [str(item) for item in range(5)]
        )


class TestTestFilters(utils.TestCase):
    """Test queryset_utils test filters templatetags."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_tests_blocking(self):
        """Test tests_blocking templatetag."""
        models.KCIDBTest.objects.update(status='P')
        self.assertQuerysetEqual(
            models.KCIDBTest.objects.none(),
            queryset_utils.tests_blocking(models.KCIDBTest.objects.all()),
        )

        models.KCIDBTest.objects.filter(id='public_test_1').update(status='F', waived=True)
        self.assertQuerysetEqual(
            models.KCIDBTest.objects.none(),
            queryset_utils.tests_blocking(models.KCIDBTest.objects.all()),
        )

        models.KCIDBTest.objects.filter(id='public_test_1').update(status='F', waived=False)
        self.assertQuerysetEqual(
            models.KCIDBTest.objects.filter(id='public_test_1'),
            queryset_utils.tests_blocking(models.KCIDBTest.objects.all()),
        )

    def test_tests_non_blocking(self):
        """Test tests_non_blocking templatetag."""
        models.KCIDBTest.objects.update(status='P')
        self.assertQuerysetEqual(
            models.KCIDBTest.objects.all(),
            queryset_utils.tests_non_blocking(models.KCIDBTest.objects.all()),
        )

        models.KCIDBTest.objects.filter(id='public_test_1').update(status='F', waived=True)
        self.assertQuerysetEqual(
            models.KCIDBTest.objects.all(),
            queryset_utils.tests_non_blocking(models.KCIDBTest.objects.all()),
        )

        models.KCIDBTest.objects.filter(id='public_test_1').update(status='F', waived=False)
        self.assertQuerysetEqual(
            models.KCIDBTest.objects.exclude(id='public_test_1'),
            queryset_utils.tests_non_blocking(models.KCIDBTest.objects.all()),
        )


class TestUrlReplace(utils.TestCase):
    """Test url_replace templatetags."""

    def test_url_replace_none(self):
        """Test url_replace without kwargs."""
        query = QueryDict('foo=foo&bar=bar')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_replace(context)
        self.assertEqual('foo=foo&bar=bar', new_url)

    def test_url_replace(self):
        """Test url_replace with kwargs. If the value exists it should replace it."""
        query = QueryDict('foo=foo&bar=bar')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_replace(context, bar='barbar')
        self.assertEqual('foo=foo&bar=barbar', new_url)

    def test_url_add(self):
        """Test url_replace with kwargs. If the value doesn't exit it should add it."""
        query = QueryDict('foo=foo')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_replace(context, bar='bar')
        self.assertEqual('foo=foo&bar=bar', new_url)

    def test_url_remove_param(self):
        """Test url_remove_param."""
        query = QueryDict('foo=foo&bar=bar')

        context = {'request': mock.Mock(GET=query)}

        new_url = url_replace.url_remove_param(context, 'bar', 'baz')
        self.assertEqual('foo=foo', new_url)


class TestSerialize(utils.TestCase):
    """Test serialize templatetags."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issue_with_kcidb.yaml',
    ]

    def test_serialize(self):
        """Test serialize templatetag."""
        issue = models.Issue.objects.first()
        serialized = serialize.serialize(issue, 'IssueSerializer')

        self.assertEqual(
            serialized,
            '{'
            '"id": 3, '
            '"kind": {"id": 1, "description": "Test issue kind", "tag": ""}, '
            '"description": "Issue Public 3", '
            '"ticket_url": "https://issue.public.3", '
            '"resolved": false, '
            '"resolved_at": null, '
            '"policy": null, '
            '"first_seen": "2020-06-03 15:14:57.215000+00:00", '
            '"last_seen": "2020-06-03 16:14:57.215000+00:00"'
            '}'
        )


class TestAvatar(utils.TestCase):
    """Test avatar templatetags."""

    @override_settings(EXTERNAL_AVATAR_URL='https://www.avatar.com/avatar')
    @override_settings(EXTERNAL_AVATAR_DEFAULT='mp')
    def test_avatar_url(self):
        """Test avatar_url does the correct request."""
        url = avatar.avatar_url('some@mail.com')
        self.assertEqual(
            'https://www.avatar.com/avatar/33fd8b0f70bc09555b992d8ac8db4459?d=mp&s=40',
            url
        )

    @override_settings(FF_EXTERNAL_AVATAR_ENABLED=False)
    def test_avatar_url_disabled(self):
        """Test avatar_url returns the default image when disabled."""
        url = avatar.avatar_url('some@mail.com')
        self.assertEqual(
            settings.DEFAULT_USER_IMAGE,
            url
        )


class TestArtifacts(utils.TestCase):
    """Test artifacts templatetags."""

    @override_settings(ARTIFACTS_DEFAULT_VALID_FOR_DAYS=10)
    def test_older_than_artifacts_default_expiry(self):
        """Test older_than_artifacts_default_expiry method."""
        test_cases = [
            (1, False),
            (9, False),
            (11, True),
            (20, True)
        ]

        for days_delta, expected in test_cases:
            timestamp = timezone.now() - datetime.timedelta(days=days_delta)
            self.assertEqual(expected, artifacts.older_than_artifacts_default_expiry(timestamp))


class TestStrUtils(utils.TestCase):
    """Test strutils templatetags."""

    def test_addstr(self):
        """Test addstr."""
        cases = [
            (1, 2, '12'),
            ('1', 2, '12'),
            ('1', '2', '12')
        ]
        for str1, str2, expected in cases:
            self.assertEqual(strutils.addstr(str1, str2), expected)

    def test_git_url_trim(self):
        """Test git_url_trim."""
        cases = [
            ('https://git.kernel.org/pub/scm/linux/kernel/git/bpf/bpf-next.git',
             'https://git.kernel.org/.../bpf/bpf-next.git'),
            ('https://git.kernel.org/kernel/git/bpf/bpf-next.git',
             'https://git.kernel.org/.../bpf/bpf-next.git'),
            ('https://git.kernel.org/bpf-next.git',
             'https://git.kernel.org/bpf-next.git'),
            ('foo/bar', 'foo/bar'),
            (None, None)
        ]
        for full, expected in cases:
            self.assertEqual(strutils.git_url_trim(full), expected)

    def test_remove_prefix(self):
        """Test remove_prefix."""
        cases = [
            ('foo-bar', 'foo', 'bar'),
            ('foo--bar', 'foo', '-bar'),
            ('foo_bar', 'foo', 'bar'),
            ('foo_bar', 'foo_', 'bar'),
            ('foobar', 'foo', 'bar'),
            ('foo_bar', 'foo-', 'foo_bar'),
            ('baz-bar', 'foo', 'baz-bar'),
        ]
        for full, prefix, expected in cases:
            self.assertEqual(strutils.remove_prefix(full, prefix), expected)
