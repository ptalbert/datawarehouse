# pylint: disable=too-few-public-methods
"""Views."""
from collections import defaultdict
import datetime
import email.utils
import mailbox

from cki_lib.kcidb.validate import validate_kcidb
from cki_lib.logger import get_logger
from cki_lib.misc import get_nested_key
from django.db.models import Prefetch
from django.shortcuts import get_object_or_404
from django.utils import timezone
from natsort import natsorted
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from datawarehouse import models
from datawarehouse import pagination
from datawarehouse import recipients
from datawarehouse import serializers as dw_serializers
from datawarehouse import utils

from . import serializers

LOGGER = get_logger(__name__)


class Submit(APIView):
    """Submit KCIDB data."""

    queryset = models.KCIDBCheckout.objects.none()

    @staticmethod
    def sorted(objects):
        """
        Sort objects by (id, misc/rerun_index).

        If no rerun_index is available, only id is used.
        """
        return natsorted(objects, key=lambda i: (i['id'], get_nested_key(i, 'misc/rerun_index', 0)))

    def post(self, request):
        """Post json data."""
        response = defaultdict(list)

        data_models = (
            ('checkouts', models.KCIDBCheckout),
            ('builds', models.KCIDBBuild),
            ('tests', models.KCIDBTest),
        )

        input_data = request.data.get('data')

        data_version = input_data.get('version')
        if data_version != {'major': 4, 'minor': 0}:
            LOGGER.info('Submitted unknown schema version: %s', input_data.get('version'))
            response['errors'].append('Unknown schema version.')
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        try:
            validate_kcidb(input_data)
        # don't want to add jsonschema dependency to catch jsonschema.exceptions.ValidationError
        # pylint: disable=broad-except
        except Exception as error:
            error_text = f'Submitted invalid kcidb schema: {repr(error)}'
            LOGGER.info(error_text)
            response['errors'].append(error_text)
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        for key, model in data_models:
            if not (sorted_data := self.sorted(input_data.get(key, []))):
                LOGGER.debug("[%r] no %s to create", request, key)
                continue

            for i, data in enumerate(sorted_data):
                if i % 100 == 0:
                    LOGGER.debug("[%r] Processing %s [%d/%d]...",
                                 request, key, i+1, len(sorted_data))

                try:
                    model.create_from_json(data)
                except models.MissingParent as error:
                    response['errors'].append([key, data['id'], str(error)])

            LOGGER.debug("[%r] Processed %d %s. There were %d errors.",
                         request, len(sorted_data), key, len(response['errors']))

        if response.get('errors'):
            LOGGER.info('Errors on submission: %s', response['errors'])
            response_status = status.HTTP_400_BAD_REQUEST
        else:
            response_status = status.HTTP_201_CREATED

        LOGGER.info('action="kcidb submit" user="%s"', request.user.username)

        return Response(response, status=response_status)


class CheckoutGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Checkout."""

    serializer_class = serializers.KCIDBCheckoutSerializer
    queryset = models.KCIDBCheckout.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class CheckoutList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of Checkouts."""

    serializer_class = serializers.KCIDBCheckoutSerializer
    queryset = models.KCIDBCheckout.objects.all()


class BuildGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Build."""

    serializer_class = serializers.KCIDBBuildSerializer
    queryset = models.KCIDBBuild.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class BuildList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of Builds for a given Checkout."""

    serializer_class = serializers.KCIDBBuildSerializer
    queryset = models.KCIDBBuild.objects.all()
    lookup_fields = (
        ('id', 'checkout__%%id'),
    )


class TestGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Test."""

    serializer_class = serializers.KCIDBTestSerializer
    queryset = models.KCIDBTest.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class TestList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of Tests run on a given Build."""

    serializer_class = serializers.KCIDBTestSerializer
    queryset = models.KCIDBTest.objects.all().select_related(
        "test",
        "origin",
        "environment",
    ).prefetch_related(
        Prefetch('build', queryset=models.KCIDBBuild.objects.only("id", "iid", "checkout_id")),
        # NOTE: checkout.kernel_type is currently used to compute "is_public".
        Prefetch('build__checkout', queryset=models.KCIDBCheckout.objects.only("iid", "kernel_type")),
        Prefetch('output_files', queryset=models.Artifact.objects.only("url", "name")),
        Prefetch('log', queryset=models.Artifact.objects.only("url")),
        Prefetch('issueoccurrence_set', queryset=models.IssueOccurrence.objects.only("id")),
        Prefetch('kcidbtestresult_set', queryset=models.KCIDBTestResult.objects.prefetch_related(
            Prefetch('output_files', queryset=models.Artifact.objects.only("url", "name"))
        )),
        "provenance",
        "test__maintainers",
    )
    lookup_fields = (
        ('id', 'build__%%id'),
    )


class TestResultGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single TestResult."""

    serializer_class = serializers.KCIDBTestResultSerializer
    queryset = models.KCIDBTestResult.objects.all()
    lookup_fields = (
        ('id', '%%id'),
    )


class TestResultList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get the list of TestResults run on a given Test."""

    serializer_class = serializers.KCIDBTestResultSerializer
    queryset = models.KCIDBTestResult.objects.all()
    lookup_fields = (
        ('id', 'test__%%id'),
    )


class KCIDBIssueView(APIView):
    """
    KCIDB Issue management.

    This endpoint should be deprecated and replaced by IssueOccurrenceView
    which contains information about the IssueOccurrence instead of just
    the Issue.
    """

    def _get_object(self, id):
        """Get object. Filter using iid or id depending on the type of {id}."""
        # Exclude objects without authorization
        queryset = self.model.objects.filter_authorized(self.request)

        return get_object_or_404(queryset, **utils.query_id_or_iid(id))

    def get(self, request, id, issue_id=None, **kwargs):
        """Get or list depending on issue_id being present."""
        if issue_id:
            return self._get(request, id, issue_id)
        else:
            return self._list(request, id)

    def _list(self, request, id, issue_id=None):
        obj = self._get_object(id)
        obj_issues = obj.issues.all()

        paginator = pagination.DatawarehousePagination()
        paginated = paginator.paginate_queryset(obj_issues, request)

        serialized = dw_serializers.IssueSerializer(paginated, many=True).data
        return paginator.get_paginated_response(serialized)

    def _get(self, request, id, issue_id=None):
        obj = self._get_object(id)
        obj_issue = get_object_or_404(obj.issues, id=issue_id)

        serialized = dw_serializers.IssueSerializer(obj_issue).data
        return Response(serialized)

    def post(self, request, id):
        """Mark checkout issues."""
        issue_id = request.data.get('issue_id')

        obj = self._get_object(id)
        issue = get_object_or_404(models.Issue, id=issue_id)

        utils.create_issue_occurrence(issue, obj, request)

        serialized = dw_serializers.IssueSerializer(issue).data
        LOGGER.info('action="add issue on %s" user="%s" issue_id="%s" iid="%s"',
                    self.queryset.model.__name__, request.user.username, issue.id, obj.iid)
        return Response(serialized, status=status.HTTP_201_CREATED)

    def delete(self, request, id, issue_id):
        """Delete Issue relationship."""
        obj = self._get_object(id)
        issue = get_object_or_404(models.Issue, id=issue_id)
        obj.issues.remove(issue)

        LOGGER.info('action="remove issue on %s" user="%s" issue_id="%s" iid="%s"',
                    self.queryset.model.__name__, request.user.username, issue.id, obj.iid)
        return Response(status=status.HTTP_204_NO_CONTENT)


class KCIDBCheckoutIssue(KCIDBIssueView):
    """KCIDBCheckout Issue management."""

    queryset = models.KCIDBCheckout.objects.none()
    model = models.KCIDBCheckout


class KCIDBBuildIssue(KCIDBIssueView):
    """KCIDBBuild Issue management."""

    queryset = models.KCIDBBuild.objects.none()
    model = models.KCIDBBuild


class KCIDBTestIssue(KCIDBIssueView):
    """KCIDBTest Issue management."""

    queryset = models.KCIDBTest.objects.none()
    model = models.KCIDBTest


class KCIDBTestResultIssue(KCIDBIssueView):
    """KCIDBTestResult Issue management."""

    queryset = models.KCIDBTestResult.objects.none()
    model = models.KCIDBTestResult


class CheckoutReport(APIView):
    """Checkout's reports."""

    queryset = models.Report.objects.none()

    def _get_object(self, id):
        # Exclude objects without authorization
        checkouts = models.KCIDBCheckout.objects.filter_authorized(self.request)
        return get_object_or_404(checkouts, **utils.query_id_or_iid(id))

    def get(self, request, id, report_id=None, **kwargs):
        """Get or list depending on issue_id being present."""
        if report_id:
            return self._get(request, id, report_id)
        else:
            return self._list(request, id)

    def _list(self, request, id, issue_id=None):
        checkout = self._get_object(id)
        reports = checkout.reports.all()

        paginator = pagination.DatawarehousePagination()
        paginated = paginator.paginate_queryset(reports, request)

        serialized = dw_serializers.ReportSerializer(paginated, many=True).data
        return paginator.get_paginated_response(serialized)

    def _get(self, request, id, report_id):
        checkout = self._get_object(id)
        if report_id.isnumeric():
            report = get_object_or_404(checkout.reports, id=report_id)
        else:
            report_id = email.utils.parseaddr(report_id)[1]
            report = get_object_or_404(checkout.reports, msgid=report_id)

        serialized = dw_serializers.ReportSerializer(report).data
        return Response(serialized)

    def post(self, request, id):
        """Submit a new report for the pipeline."""
        content = request.data.get('content')
        mbox = mailbox.mboxMessage(content)

        addr_to = mbox.get_all('to', [])
        addr_cc = mbox.get_all('cc', [])
        subject = mbox.get('Subject')
        date = mbox.get('Date')
        msgid = email.utils.parseaddr(mbox.get('Message-ID'))[1]

        if models.Report.objects.filter(msgid=msgid).exists():
            return Response("MsgID already exists.", status=status.HTTP_400_BAD_REQUEST)

        checkout = self._get_object(id)

        if mbox.is_multipart():
            for part in mbox.walk():
                ctype = part.get_content_type()
                cdispo = str(part.get('Content-Disposition'))

                if ctype == 'text/plain' and 'attachment' not in cdispo:
                    body = part.get_payload(decode=True).decode()
                    break
        else:
            body = mbox.get_payload(decode=True).decode()

        sent_at = email.utils.parsedate_to_datetime(date)
        try:
            sent_at = timezone.make_aware(sent_at)
        except ValueError:
            # tzinfo is already set
            pass

        report = models.Report.objects.create(
            kcidb_checkout=checkout,
            subject=subject,
            body=body,
            msgid=msgid,
            sent_at=sent_at,
            raw=content,
        )

        for _, addr in email.utils.getaddresses(addr_to):
            recipient, _ = models.Recipient.objects.get_or_create(email=addr)
            report.addr_to.add(recipient)

        for _, addr in email.utils.getaddresses(addr_cc):
            recipient, _ = models.Recipient.objects.get_or_create(email=addr)
            report.addr_cc.add(recipient)

        serialized_report = dw_serializers.ReportSerializer(report).data

        LOGGER.info('action="new report on KCIDBCheckout" user="%s" report_id="%s" iid="%s"',
                    request.user.username, report.id, checkout.iid)

        return Response(serialized_report, status=status.HTTP_201_CREATED)


class CheckoutReportMissing(APIView):
    """Get checkouts without reports sent."""

    queryset = models.Report.objects.none()

    def get(self, request):
        """Get list of checkouts without reports."""
        since = request.GET.get('since')

        # Until we can determine when a checkout finished,
        # just exclude those from the last 24 hs.
        last_24_hours = timezone.now() - datetime.timedelta(hours=24)

        checkouts = (
            models.KCIDBCheckout.objects
            .filter_authorized(self.request)
            .filter(reports=None)
            .exclude(start_time__gte=last_24_hours)
        )

        if since:
            since = utils.timestamp_to_datetime(since)
            checkouts = checkouts.filter(start_time__gte=since)

        paginator = pagination.DatawarehousePagination()
        paginated = paginator.paginate_queryset(checkouts, request)

        serialized = serializers.KCIDBCheckoutSerializer(paginated, many=True).data
        return paginator.get_paginated_response(serialized)


class ReportGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single report."""

    serializer_class = dw_serializers.ReportSerializer
    queryset = models.Report.objects.all()

    def filter_queryset(self, request):
        """Filter report by id or msg_id."""
        report_id = self.kwargs['report_id']
        if report_id.isnumeric():
            return self.queryset.filter(id=report_id)
        report_id = email.utils.parseaddr(report_id)[1]
        return self.queryset.filter(msgid=report_id)


class ReportCheckoutGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get report's checkout."""

    serializer_class = serializers.KCIDBCheckoutSerializer
    queryset = models.KCIDBCheckout.objects.all()

    def filter_queryset(self, request):
        """Filter report by id or msg_id."""
        report_id = self.kwargs['report_id']
        if report_id.isnumeric():
            return self.queryset.filter(reports__id=report_id)
        report_id = email.utils.parseaddr(report_id)[1]
        return self.queryset.filter(reports__msgid=report_id)


class ObjectActionTriaged(APIView):
    """Tag KCIDB object as triaged."""

    def _get_object(self, id):
        """Get object. Filter using iid or id depending on the type of {id}."""
        queryset = self.queryset.model.objects.filter_authorized(self.request)
        return get_object_or_404(queryset, **utils.query_id_or_iid(id))

    def post(self, request, id):
        """Tag object as triaged."""
        obj = self._get_object(id)
        obj.last_triaged_at = timezone.now()
        obj.save()
        return Response(status=201)


class CheckoutActionTriaged(ObjectActionTriaged):
    """Tag KCIDBCheckout as triaged."""

    queryset = models.KCIDBCheckout.objects.none()


class BuildActionTriaged(ObjectActionTriaged):
    """Tag KCIDBBuild as triaged."""

    queryset = models.KCIDBBuild.objects.none()


class TestActionTriaged(ObjectActionTriaged):
    """Tag KCIDBTest as triaged."""

    queryset = models.KCIDBTest.objects.none()


class TestResultActionTriaged(ObjectActionTriaged):
    """Tag KCIDBTestResult as triaged."""

    queryset = models.KCIDBTestResult.objects.none()


class CheckoutRecipients(APIView):
    """List rendered recipients for a Checkout."""

    queryset = models.KCIDBCheckout.objects.none()

    def get(self, request, id, **kwargs):
        """Get recipients for the given KCIDBCheckout."""
        # First filter authorized checkout and then run final query as it's not
        # possible to chain filter_authorized() and aggregated().
        checkout = get_object_or_404(
            models.KCIDBCheckout.objects.filter_authorized(self.request),
            **utils.query_id_or_iid(id)
        )
        checkout = models.KCIDBCheckout.objects.aggregated().get(iid=checkout.iid)

        return Response(recipients.Recipients(checkout).render())


class CheckoutAll(APIView):
    """Get checkout and all the child objects."""

    queryset = models.KCIDBCheckout.objects.none()

    def get(self, request, id, **kwargs):
        """Get recipients for the given KCIDBCheckout."""
        queryset = models.KCIDBCheckout.objects.filter_authorized(self.request)

        checkout = get_object_or_404(queryset, **utils.query_id_or_iid(id))
        builds = (
            checkout.kcidbbuild_set.all()
            .select_related(
                'checkout',
                'compiler',
                'origin',
                'log',
            ).prefetch_related(
                'input_files',
                'output_files',
                'provenance',
            )
        )

        tests = (
            models.KCIDBTest.objects
            .filter(
                build__in=builds,
            ).select_related(
                'build',
                'origin',
                'environment',
                'test',
                'log',
            ).prefetch_related(
                'output_files',
                'issueoccurrence_set',
            )
        )

        testresults = (
            models.KCIDBTestResult.objects
            .filter(
                test__in=tests,
            ).select_related(
                'test',
            ).prefetch_related(
                'output_files',
                'issueoccurrence_set',
            )
        )

        issueoccurrences = models.IssueOccurrence.objects.filter(
            related_checkout__iid=checkout.iid,
        ).select_related(
            'issue__policy',
            'issue__kind',
        ).prefetch_related(
            'issue',
        )

        response = {
            'checkouts': serializers.KCIDBCheckoutSerializer([checkout], many=True).data,
            'builds': serializers.KCIDBBuildSerializer(builds, many=True).data,
            'tests': serializers.KCIDBTestLightSerializer(tests, many=True).data,
            'testresults': serializers.KCIDBTestResultLightSerializer(testresults, many=True).data,
            'issueoccurrences': dw_serializers.IssueOccurrenceLightSerializer(issueoccurrences, many=True).data,
        }

        return Response(response)


class IssueOccurrenceView(APIView):
    """Issue occurrences management."""

    def _get_object(self, id):
        """Get object. Filter using iid or id depending on the type of {id}."""
        # Exclude objects without authorization
        queryset = self.model.objects.filter_authorized(self.request)

        return get_object_or_404(queryset, **utils.query_id_or_iid(id))

    @staticmethod
    def _get_issue_occurrences(obj):
        """
        Return the IssueOccurrences for a given object.

        Use the ManyRelatedManager approach to transparently get the "through"
        objects for the KCIDB object -> Issue relationship.

        ManyRelatedManager.pk_field_names is a dictionary where the key is the
        name of the object and the value is the name of the field.
        """
        return models.IssueOccurrence.objects.filter(**{
            f'{key}__{value}': getattr(obj, value)
            for key, value in obj.issues.pk_field_names.items()
        })

    def get(self, request, id, issue_id=None, **kwargs):
        """Get or list depending on issue_id being present."""
        if issue_id:
            return self._get(request, id, issue_id)
        else:
            return self._list(request, id)

    def _list(self, request, id):
        obj = self._get_object(id)
        obj_issues = self._get_issue_occurrences(obj)

        paginator = pagination.DatawarehousePagination()
        paginated = paginator.paginate_queryset(obj_issues, request)

        serialized = dw_serializers.IssueOccurrenceSerializer(paginated, many=True).data
        return paginator.get_paginated_response(serialized)

    def _get(self, request, id, issue_id):
        obj = self._get_object(id)
        obj_issues = self._get_issue_occurrences(obj)
        obj_issue = get_object_or_404(obj_issues, issue__id=issue_id)

        serialized = dw_serializers.IssueOccurrenceSerializer(obj_issue).data
        return Response(serialized)


class CheckoutIssueOccurrence(IssueOccurrenceView):
    """Checkout Issue occurrences management."""

    queryset = models.KCIDBCheckout.objects.none()
    model = models.KCIDBCheckout


class BuildIssueOccurrence(IssueOccurrenceView):
    """Build Issue occurrences management."""

    queryset = models.KCIDBBuild.objects.none()
    model = models.KCIDBBuild


class TestIssueOccurrence(IssueOccurrenceView):
    """Test Issue occurrences management."""

    queryset = models.KCIDBTest.objects.none()
    model = models.KCIDBTest


class TestResultIssueOccurrence(IssueOccurrenceView):
    """TestResult Issue occurrences management."""

    queryset = models.KCIDBTestResult.objects.none()
    model = models.KCIDBTestResult
