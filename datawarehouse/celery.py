# pylint: disable=invalid-name
"""Celery config."""
import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'datawarehouse.settings')

app = Celery('datawarehouse')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
