# pylint: disable=too-many-lines
"""KCIDB schema models file."""
# pylint: disable=too-many-lines
from email.utils import parseaddr
import json

from cki_lib.misc import get_nested_key
from django.conf import settings
from django.db import models
from django.db.models import Prefetch
from django.forms.models import model_to_dict
from django.urls import reverse
from django.utils.functional import cached_property
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import metrics
from datawarehouse import utils
from datawarehouse.models import authorization_models
from datawarehouse.models import file_models
from datawarehouse.models import issue_models
from datawarehouse.models import patch_models
from datawarehouse.models import pipeline_models
from datawarehouse.models import test_models
from datawarehouse.models.utils import ActionsDataModel
from datawarehouse.models.utils import GenericNameManager
from datawarehouse.models.utils import Manager
from datawarehouse.models.utils import Model


class ObjectStatusEnum(models.TextChoices):
    """Status of objects in messages."""

    NEW = 'new'
    NEEDS_TRIAGE = 'needs_triage'
    READY_TO_REPORT = 'ready_to_report'
    UPDATED = 'updated'
    BUILD_SETUPS_FINISHED = 'build_setups_finished'
    TESTS_FINISHED = 'tests_finished'


class KernelTypeEnum(models.TextChoices):
    """Kernel type."""

    INTERNAL = 'i'
    UPSTREAM = 'u'


class ProvenanceComponentFunctionEnum(models.TextChoices):
    """Provenance component function."""

    COORDINATOR = 'c', 'coordinator'
    PROVISIONER = 'p', 'provisioner'
    EXECUTOR = 'e', 'executor'


class MissingParent(Exception):
    """Object's parent is missing."""

    def __init__(self, obj_cls, obj_id):
        """Craft exception message."""
        super().__init__(f'{obj_cls.__name__} id={obj_id} is not present in the DB')


class Maintainer(EMOM('maintainer'), models.Model):
    """Model for Maintainer."""

    name = models.CharField(max_length=100)
    email = models.EmailField()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} <{self.email}>'

    @classmethod
    def create_from_address(cls, address):
        """Create Maintainer from address."""
        name, email = parseaddr(address)
        maintainer = cls.objects.update_or_create(
            email=email,
            defaults={'name': name},
        )[0]
        return maintainer


class ProvenanceComponent(EMOM('provenance_component'), models.Model):
    """Model for a provenance component."""

    url = models.URLField(unique=True)
    function = models.CharField(max_length=1, choices=ProvenanceComponentFunctionEnum.choices)
    environment = models.JSONField(blank=True, null=True)
    service_name = models.CharField(max_length=20, blank=True, null=True)

    @classmethod
    def create_from_dict(cls, data):
        """Create ProvenanceComponent from dict."""
        provenance_obj, _ = cls.objects.get_or_create(
            url=data['url'],
            defaults={
                'function': ProvenanceComponentFunctionEnum[data['function'].upper()],
                'environment': data.get('environment'),
                'service_name': data.get('service_name'),
            }
        )
        return provenance_obj

    def __str__(self):
        """Return __str__ formatted."""
        if self.service_name == 'gitlab':
            if '/jobs/' in self.url:
                return 'Gitlab Job'
            if '/pipelines/' in self.url:
                return 'Gitlab Pipeline'

        elif self.service_name == 'beaker':
            if '/recipes/' in self.url:
                return 'Beaker Recipe'

        return self.get_function_display()


class KCIDBOrigin(EMOM('kcidb_origin'), models.Model):
    """Model for KCIDBOrigin."""

    name = models.CharField(max_length=100, unique=True)

    objects = GenericNameManager()

    def __str__(self):
        """Return __str__ formatted."""
        return self.name

    @classmethod
    def create_from_string(cls, name):
        """Create KCIDBOrigin from string."""
        return cls.objects.get_or_create(name=name)[0]


class KCIDBCheckoutManager(Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBCheckout."""

    def aggregated(self):
        # pylint: disable=too-many-locals
        """Add aggregated information."""
        checkout = KCIDBCheckout.objects.filter(iid=models.OuterRef('iid'))
        checkout_untriaged = checkout.filter(
            valid=False,
            issues=None
        )

        builds_run = KCIDBBuild.objects.filter(checkout__iid=models.OuterRef('iid'))
        builds_running = builds_run.filter(valid=None)
        builds_pass = builds_run.filter(valid=True)
        builds_fail = builds_run.filter(valid=False)
        builds_untriaged = builds_fail.filter(issues=None)

        tests_run = KCIDBTest.objects.filter(build__checkout__iid=models.OuterRef('iid'))
        tests_running = tests_run.filter(status=None)
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL).exclude(waived=True)
        tests_error = tests_run.filter(status=test_models.ResultEnum.ERROR)
        tests_untriaged = (
            KCIDBTest.objects
            .filter(
                build__checkout__iid=models.OuterRef('iid'),
                status__in=KCIDBTest.UNSUCCESSFUL_STATUSES,
                issues=None
            )
        )

        counts = {
            'builds_run': builds_run,
            'builds_running': builds_running,
            'builds_pass': builds_pass,
            'builds_fail': builds_fail,
            'tests_run': tests_run,
            'tests_running': tests_running,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
            'tests_error': tests_error,
        }

        # Look for issues to determine if there are objects triaged.
        checkout_issues = issue_models.Issue.objects.filter(
            kcidbcheckout__iid=models.OuterRef('iid')
        )
        build_issues = issue_models.Issue.objects.filter(
            kcidbbuild__checkout__iid=models.OuterRef('iid')
        )
        test_issues = issue_models.Issue.objects.filter(
            kcidbtest__build__checkout__iid=models.OuterRef('iid')
        )

        annotations = {
            # Child objects passed
            'stats_builds_passed': ~models.Exists(builds_fail),
            'stats_tests_passed': ~models.Exists(tests_fail),

            # Objects have issues
            'stats_checkout_triaged': models.Exists(checkout_issues),
            'stats_builds_triaged': models.Exists(build_issues),
            'stats_tests_triaged': models.Exists(test_issues),

            # Objects have failures without issues
            'stats_checkout_untriaged': models.Exists(checkout_untriaged),
            'stats_builds_untriaged': models.Exists(builds_untriaged),
            'stats_tests_untriaged': models.Exists(tests_untriaged),

            # Checkout has tests with errors
            'stats_tests_errors': models.Exists(tests_error),
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('iid').order_by().annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def annotated_by_architecture(self):
        # pylint: disable=too-many-locals
        """Add build and test information by architecture."""
        counts = {}
        for arch in pipeline_models.ArchitectureEnum:
            builds_run = (
                KCIDBBuild.objects
                .filter(checkout__iid=models.OuterRef('iid'), architecture=arch)
                .exclude(valid=None)
            )
            builds_fail = builds_run.filter(valid=False)
            tests_run = (
                KCIDBTest.objects
                .filter(build__checkout__iid=models.OuterRef('iid'), build__architecture=arch)
                .exclude(status=None)
            )
            tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL)

            counts.update({
                f'{arch.name}_builds_ran': builds_run,
                f'{arch.name}_builds_failed': builds_fail,
                f'{arch.name}_builds_failed_untriaged': builds_fail.filter(issues=None),
                f'{arch.name}_builds_with_issues': builds_fail.exclude(issues=None),
                f'{arch.name}_tests_ran': tests_run,
                f'{arch.name}_tests_failed': tests_fail.exclude(waived=True),
                f'{arch.name}_tests_failed_waived': tests_fail.filter(waived=True),
                f'{arch.name}_tests_failed_untriaged': tests_fail.exclude(waived=True).filter(issues=None),
                f'{arch.name}_tests_with_issues': tests_run.exclude(issues=None),
            })

        annotations = {}
        for name, query in counts.items():
            count = query.values('iid').order_by().annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def filter_ready_to_report(self):
        """
        Get checkouts ready to report.

        To match the 'ready_to_report' condition, a checkout must match
        all the following conditions:

        - Not be already tagged ready_to_report (ready_to_report == False).
        - Checkout valid != None (finished) and last_triaged_at != None (triaged).
        - If Builds exist, all Builds valid != None (finished) and last_triaged_at != None (triaged).
        - If Test exist, all Tests status != None (finished) and last_triaged_at != None (triaged).
        """
        annotations = {
            'unfinished_checkout': models.Exists(KCIDBCheckout.objects.filter(
                models.Q(iid=models.OuterRef('iid')) & (
                    models.Q(valid=None) |
                    models.Q(last_triaged_at=None)
                )
            )),
            'unfinished_builds': models.Exists(KCIDBBuild.objects.filter(
                models.Q(checkout__iid=models.OuterRef('iid')) & (
                    models.Q(valid=None) |
                    models.Q(last_triaged_at=None)
                )
            )),
            'missing_testplan': (
                # There are not KCIDBBuild with test_plan_missing, as long as there are no failed builds.
                models.ExpressionWrapper(
                    models.Exists(KCIDBBuild.objects.filter(
                        models.Q(checkout__iid=models.OuterRef('iid')) & models.Q(test_plan_missing=True)
                    )) & ~ models.Exists(KCIDBBuild.objects.filter(
                        models.Q(checkout__iid=models.OuterRef('iid')) & models.Q(valid=False)
                    )),
                    output_field=models.BooleanField()
                )
            ),
            'unfinished_tests': models.Exists(KCIDBTest.objects.filter(
                models.Q(build__checkout__iid=models.OuterRef('iid')) & (
                    models.Q(status=None) |
                    models.Q(last_triaged_at=None)
                )
            )),
        }

        return (
            self
            .filter(ready_to_report=False)
            .annotate(**annotations)
            .exclude(
                models.Q(unfinished_checkout=True) |
                models.Q(unfinished_builds=True) |
                models.Q(missing_testplan=True) |
                models.Q(unfinished_tests=True)
            )
        )

    def filter_build_setups_finished(self):
        """Get checkouts with all the build setups finished.

        To achieve this match, we're checking the following conditions:
        - notification_sent_build_setups_finished must be False
        - Checkout must have any Build, and all Builds must have kpet_tree_name != None
        """
        annotations = {
            'unfinished_build_setups': models.Exists(KCIDBBuild.objects.filter(
                (models.Q(checkout__iid=models.OuterRef('iid')) & models.Q(kpet_tree_name=None))
                )
            ),
            'has_builds': models.Exists(KCIDBBuild.objects.filter(
                models.Q(checkout__iid=models.OuterRef('iid'))
                )
            ),
        }

        return (
            self
            .filter(notification_sent_build_setups_finished=False)
            .annotate(**annotations)
            .exclude(models.Q(has_builds=False))
            .exclude(models.Q(unfinished_build_setups=True))
        )

    def filter_tests_finished(self):
        """Get checkout with all tests finished.

        To achieve this match, we're checking the following conditions:
        - notification_sent_build_setups_finished must be False
        - Checkout must have any Test, and all Tests status != None (finished).
        - Checkout must not have Builds with test_plan_missing.
        """
        annotations = {
            'unfinished_tests': models.Exists(KCIDBTest.objects.filter(
                models.Q(build__checkout__iid=models.OuterRef('iid')) & models.Q(status=None)
                )
            ),
            'has_tests': models.Exists(KCIDBTest.objects.filter(
                models.Q(build__checkout__iid=models.OuterRef('iid'))
                )
            ),
            'has_builds_with_test_plan_missing': models.Exists(KCIDBBuild.objects.filter(
                models.Q(checkout__iid=models.OuterRef('iid')) & models.Q(test_plan_missing=True)
                )
            ),
        }

        return (
            self
            .filter(notification_sent_tests_finished=False)
            .annotate(**annotations)
            .exclude(models.Q(has_tests=False))
            .exclude(models.Q(unfinished_tests=True))
            .exclude(models.Q(has_builds_with_test_plan_missing=True))
        )

    def filter_has_builds(self):
        """Get checkout with any build."""
        annotations = {
            'has_builds': models.Exists(KCIDBBuild.objects.filter(
                models.Q(checkout__iid=models.OuterRef('iid'))
                )
            ),
        }

        return (
            self
            .annotate(**annotations)
            .exclude(models.Q(has_builds=False))
        )

    def filter_has_tests(self):
        """Get checkout with any test."""
        annotations = {
            'has_tests': models.Exists(KCIDBTest.objects.filter(
                models.Q(build__checkout__iid=models.OuterRef('iid'))
                )
            ),
        }

        return (
            self
            .annotate(**annotations)
            .exclude(models.Q(has_tests=False))
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBCheckout(EMOM('kcidb_checkout'), Model, ActionsDataModel):
    # pylint: disable=too-many-public-methods
    """Model for KCIDBCheckout."""

    iid = models.AutoField(primary_key=True)

    id = models.CharField(max_length=300, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    tree = models.ForeignKey('GitTree', on_delete=models.PROTECT, null=True, blank=True)
    git_repository_url = models.URLField(max_length=400, null=True, blank=True)
    git_repository_branch = models.CharField(max_length=200, null=True, blank=True)
    git_commit_hash = models.CharField(max_length=40, null=True, blank=True)
    git_commit_name = models.CharField(max_length=100, null=True, blank=True)
    patches = models.ManyToManyField('Patch', related_name='checkouts', blank=True)
    patchset_hash = models.CharField(max_length=64, null=True, blank=True)
    message_id = models.CharField(null=True, blank=True, max_length=200)
    comment = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    contacts = models.ManyToManyField('Maintainer', related_name='checkouts', blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    log_excerpt = models.TextField(null=True, blank=True)
    valid = models.BooleanField(null=True)

    # Fields not belonging to KCIDB schema
    kernel_type = models.CharField(max_length=1, choices=KernelTypeEnum.choices, null=True, blank=True)
    kernel_version = models.CharField(max_length=100, blank=True, null=True)
    source_package_name = models.CharField(max_length=100, null=True, blank=True)
    ready_to_report = models.BooleanField(default=False)

    brew_task_id = models.IntegerField(null=True, blank=True)
    submitter = models.ForeignKey('Maintainer', related_name='submitted_checkouts',
                                  on_delete=models.SET_NULL, blank=True, null=True)
    scratch = models.BooleanField(blank=True, null=True)
    retrigger = models.BooleanField(blank=True, null=True)
    all_sources_targeted = models.BooleanField(blank=True, null=True)

    patchset_modified_files = models.JSONField(blank=True, null=True)
    related_merge_request = models.JSONField(blank=True, null=True)

    notification_send_ready_for_test_post = models.BooleanField(blank=True, null=True)
    notification_send_ready_for_test_pre = models.BooleanField(blank=True, null=True)

    notification_sent_build_setups_finished = models.BooleanField(default=False)
    notification_sent_tests_finished = models.BooleanField(default=False)

    # Override ActionsDataModel.issues to add through_fields information.
    issues = models.ManyToManyField(
        to='Issue',
        through='IssueOccurrence',
        # IssueOccurrence is related to KCIDBCheckout twice, so it's
        # necessary to specify which of them should be used in through_fields.
        through_fields=('kcidb_checkout', 'issue')
    )
    report_rules = models.JSONField(null=True, blank=True, default=list)

    policy = models.ForeignKey(authorization_models.Policy, on_delete=models.PROTECT,
                               null=True, blank=True)
    provenance = models.ManyToManyField(ProvenanceComponent, blank=True, related_name='checkouts')
    objects = KCIDBCheckoutManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'policy'

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    @property
    def checkout(self):
        """
        Return the checkout for this object.

        Provide a transparent way of doing obj.checkout no matter which KCIDB object it is.
        """
        return self

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    def is_equivalent_to(self, other):
        """Compare to KCIDBCheckout object using a list of fields, otherwise False."""
        fields = ['id', 'git_repository_url', 'git_repository_branch', 'git_commit_hash', 'git_commit_name',
                  'patchset_hash', 'message_id', 'comment', 'start_time', 'log_excerpt', 'valid']
        if isinstance(other, KCIDBCheckout):
            return utils.clean_dict(model_to_dict(self, fields=fields)) ==\
                utils.clean_dict(model_to_dict(other, fields=fields))
        return False

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.checkouts', args=[self.iid])

    @property
    def is_public(self):
        """Return True if this KCIDBCheckout is public."""
        return self.kernel_type == KernelTypeEnum.UPSTREAM

    @property
    def is_triaged(self):
        """Return True if this checkout is triaged."""
        if hasattr(self, 'stats_checkout_triaged'):
            return self.stats_checkout_triaged  # pylint: disable=no-member

        return self.issues.exists()

    @property
    def is_missing_triage(self):
        """Return True if this checkout is missing triage."""
        if hasattr(self, 'stats_checkout_untriaged'):
            return self.stats_checkout_untriaged  # pylint: disable=no-member

        return not self.valid and not self.issueoccurrence_set.exists()

    @property
    def builds_triaged(self):
        """Return list of triaged builds."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_builds_triaged') and not self.stats_builds_triaged:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.exclude(issues=None)

    @property
    def builds_untriaged(self):
        """Return list of untriaged builds."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_builds_untriaged') and not self.stats_builds_untriaged:
            return KCIDBBuild.objects.none()

        return self.kcidbbuild_set.filter_untriaged()

    @property
    def tests_triaged(self):
        """Return list of triaged tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_triaged') and not self.stats_tests_triaged:
            return KCIDBTest.objects.none()

        return (
            KCIDBTest.objects
            .filter(build__checkout=self)
            .exclude(issues=None)
        )

    @property
    def tests_untriaged(self):
        """Return list of untriaged tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_untriaged') and not self.stats_tests_untriaged:
            return KCIDBTest.objects.none()

        return (
            KCIDBTest.objects
            .filter_untriaged()
            .filter(build__checkout=self)
        )

    @property
    def has_objects_missing_triage(self):
        """Return if there are failures missing triage."""
        try:
            return (
                self.stats_checkout_untriaged or
                self.stats_builds_untriaged or
                self.stats_tests_untriaged
            )
        except AttributeError:
            return None

    @property
    def has_objects_with_issues(self):
        """Return if the checkout or one of it's builds and tests has any issues."""
        try:
            return (
                self.stats_checkout_triaged or
                self.stats_builds_triaged or
                self.stats_tests_triaged
            )
        except AttributeError:
            return None

    @property
    def tests_passed(self):
        # pylint: disable=no-member
        """Return True if all tests passed."""
        if self.stats_tests_running_count:
            return None
        return not bool(self.stats_tests_fail_count)

    @property
    def is_test_plan(self):
        """Return True if is a test plan."""
        return self.valid is None

    @property
    def succeeded(self):
        """
        Assess whether the checkout was a success.

        Asumes the KCIDBCheckout was previously aggregated().
        """
        return (
            self.valid and
            self.stats_builds_passed and  # pylint: disable=no-member
            not self.has_failed_tests
        )

    @property
    def has_failed_tests(self):
        """
        Assess whether the checkout has failing tests.

        This method only returns True if the checkout has non-waived
        tests with status=Fail and without issues tagged.
        """
        return (
            KCIDBTest.objects.filter(
                build__checkout=self,
                status=test_models.ResultEnum.FAIL,
                issues=None
            ).exclude(
                waived=True,
            ).exists()
        )

    @classmethod
    def create_from_json(cls, data):
        # pylint: disable=too-many-locals
        """Create KCIDBCheckout from kcidb json."""
        try:
            existing_checkout = KCIDBCheckout.objects.get(id=data['id'], origin__name=data['origin'])
        except KCIDBCheckout.DoesNotExist:
            existing_checkout = None

        misc = data.get('misc', {})

        log_url = data.get('log_url')
        log = file_models.Artifact.create_from_url(log_url) if log_url else None

        tree_name = data.get('tree_name')
        tree = pipeline_models.GitTree.create_from_string(tree_name) if tree_name else None

        submitter = misc.get('submitter')
        submitter = Maintainer.create_from_address(submitter) if submitter else None

        origin = KCIDBOrigin.create_from_string(data['origin'])

        if report_rules := misc.get('report_rules'):
            report_rules = json.loads(report_rules)

        checkout, created = cls.objects.defer('patchset_modified_files').update_or_create(
            id=data['id'],
            origin=origin,
            # for an empty data dict, all of these have to be None
            defaults=utils.clean_dict({
                'tree': tree,
                'git_repository_url': data.get('git_repository_url'),
                'git_repository_branch': data.get('git_repository_branch'),
                'git_commit_hash': data.get('git_commit_hash'),
                'git_commit_name': data.get('git_commit_name'),
                'message_id': data.get('message_id'),
                'comment': data.get('comment'),
                'start_time': data.get('start_time'),
                'valid': data.get('valid'),
                'log': log,
                'report_rules': report_rules,
                'log_excerpt': data.get('log_excerpt'),
                'kernel_version': misc.get('kernel_version'),
                'source_package_name': misc.get('source_package_name'),
                'patchset_hash': data.get('patchset_hash'),
                'brew_task_id': misc.get('brew_task_id'),
                'submitter': submitter,
                'scratch': misc.get('scratch'),
                'retrigger': misc.get('retrigger'),
                'all_sources_targeted': misc.get('all_sources_targeted'),
                'patchset_modified_files': misc.get('patchset_modified_files'),
                'related_merge_request': misc.get('mr'),
                'notification_send_ready_for_test_pre': misc.get('send_ready_for_test_pre'),
                'notification_send_ready_for_test_post': misc.get('send_ready_for_test_post'),
            })
        )

        if created:
            # Set default values, should not be overwritten when resubmitted without these fields
            checkout.kernel_type = KernelTypeEnum[misc.get('kernel_type', 'internal').upper()]

            if get_nested_key(data, 'misc/retrigger', False):
                checkout.policy = authorization_models.Policy.objects.get(name=authorization_models.Policy.RETRIGGER)
            elif checkout.kernel_type == KernelTypeEnum.UPSTREAM:
                checkout.policy = authorization_models.Policy.objects.get(name=authorization_models.Policy.PUBLIC)
            else:
                checkout.policy = authorization_models.Policy.objects.get(name=authorization_models.Policy.INTERNAL)
            checkout.save()

        for contact in data.get('contacts', []):
            maintainer = Maintainer.create_from_address(contact)
            checkout.contacts.add(maintainer)

        patches_data = data.get('patchset_files', [])
        if patches_data:
            patches = patch_models.Patch.create_from_patchset_files(patches_data)
            checkout.patches.set(patches)

        if provenance_components := misc.get('provenance'):
            checkout.provenance.set([
                ProvenanceComponent.create_from_dict(data)
                for data in provenance_components
            ])

        utils.created_object_send_message(existing_checkout, checkout, 'checkout')

        return checkout

    @cached_property
    def annotated_by_architecture(self):
        """Return annotated fields in a dict structure for easier access in templates."""
        data = {}
        for arch in pipeline_models.ArchitectureEnum:
            arch_data = {
                'builds': {
                    'ran': getattr(self, f'stats_{arch.name}_builds_ran_count'),
                    'failed': getattr(self, f'stats_{arch.name}_builds_failed_count'),
                    'failed_untriaged': getattr(self, f'stats_{arch.name}_builds_failed_untriaged_count'),
                },
                'tests': {
                    'ran': getattr(self, f'stats_{arch.name}_tests_ran_count'),
                    'failed': getattr(self, f'stats_{arch.name}_tests_failed_count'),
                    'failed_untriaged': getattr(self, f'stats_{arch.name}_tests_failed_untriaged_count'),
                    'failed_waived': getattr(self, f'stats_{arch.name}_tests_failed_waived_count'),
                },
                'known_issues': (
                    getattr(self, f'stats_{arch.name}_builds_with_issues_count') +
                    getattr(self, f'stats_{arch.name}_tests_with_issues_count')
                )
            }
            data[arch.name] = arch_data

        return data


class KCIDBBuildManager(Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBBuild."""

    def aggregated(self):
        """Add aggregated information."""
        tests_run = KCIDBTest.objects.filter(build__id=models.OuterRef('id'))
        tests_running = tests_run.filter(status=None)
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL).exclude(waived=True)
        tests_error = tests_run.filter(status=test_models.ResultEnum.ERROR)

        tests_untriaged = (
            KCIDBTest.objects
            .filter(
                build__iid=models.OuterRef('iid'),
                status__in=KCIDBTest.UNSUCCESSFUL_STATUSES,
                issues=None
            )
        )

        counts = {
            'tests_run': tests_run,
            'tests_running': tests_running,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
            'tests_error': tests_error,
        }

        annotations = {
            'stats_tests_passed': ~models.Exists(tests_fail),
            'stats_tests_errors': models.Exists(tests_error),
            'stats_tests_untriaged': models.Exists(tests_untriaged),
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('id').order_by().annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def filter_untriaged(self):
        """Filter untriaged builds."""
        return self.filter(
            valid=False,
            issues=None
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBBuild(EMOM('kcidb_build'), Model, ActionsDataModel):
    """Model for KCIDBBuild."""

    iid = models.AutoField(primary_key=True)

    checkout = models.ForeignKey('KCIDBCheckout', on_delete=models.CASCADE)
    id = models.CharField(max_length=200, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    comment = models.TextField(null=True, blank=True)
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    command = models.TextField(null=True, blank=True)
    compiler = models.ForeignKey('Compiler', on_delete=models.PROTECT, null=True, blank=True)
    input_files = models.ManyToManyField('Artifact', related_name='build_input', blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='build_output', blank=True)
    config_name = models.CharField(max_length=50, null=True, blank=True)
    config_url = models.URLField(max_length=400, null=True, blank=True)
    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    log_excerpt = models.TextField(null=True, blank=True)
    valid = models.BooleanField(null=True)

    architecture = models.IntegerField(choices=pipeline_models.ArchitectureEnum.choices,
                                       null=True, blank=True)

    # Fields not belonging to KCIDB schema
    debug = models.BooleanField(null=True, blank=True)
    # Flag to prevent a KCIDBCheckout from being ready if a build is missing tests.
    test_plan_missing = models.BooleanField(default=False)
    retrigger = models.BooleanField(blank=True, null=True)

    kpet_tree_name = models.CharField(max_length=100, null=True, blank=True)
    kpet_tree_family = models.CharField(max_length=100, null=True, blank=True)
    package_name = models.CharField(max_length=100, null=True, blank=True)
    provenance = models.ManyToManyField(ProvenanceComponent, blank=True, related_name='builds')
    testing_skipped_reason = models.CharField(max_length=100, null=True, blank=True)

    objects = KCIDBBuildManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'checkout__policy'

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('-iid',)

    def is_equivalent_to(self, other):
        """Compare two KCIDBBuild objects using a list of fields, otherwise False."""
        fields = ['id', 'comment', 'start_time', 'duration', 'architecture', 'command', 'config_name',
                  'config_url', 'log_excerpt', 'valid']
        if isinstance(other, KCIDBBuild):
            return utils.clean_dict(model_to_dict(self, fields=fields)) == \
                utils.clean_dict(model_to_dict(other, fields=fields))
        return False

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.builds', args=[self.iid])

    @property
    def is_public(self):
        """Return True if this KCIDBBuild is public."""
        return self.checkout.kernel_type == KernelTypeEnum.UPSTREAM

    @property
    def tests_passed(self):
        """Return True if all tests passed."""
        # pylint: disable=no-member
        if self.stats_tests_running_count:
            return None
        try:
            return not bool(self.stats_tests_fail_count)
        except AttributeError:
            return not self.kcidbtest_set.filter(status=test_models.ResultEnum.FAIL).exclude(waived=True).exists()

    @property
    def has_tests_errors(self):
        """Return True if a test had errors."""
        try:
            return self.stats_tests_errors
        except AttributeError:
            return self.kcidbtest_set.filter(status=test_models.ResultEnum.ERROR).exists()

    @property
    def tests_untriaged(self):
        """Return list of untriaged tests."""
        # pylint: disable=no-member
        if hasattr(self, 'stats_tests_untriaged') and not self.stats_tests_untriaged:
            return KCIDBTest.objects.none()

        return (
            KCIDBTest.objects
            .filter_untriaged()
            .filter(build=self)
        )

    @property
    def all_passed(self):
        """Return True if the build and tests passed."""
        return self.valid and self.tests_passed

    @property
    def is_test_plan(self):
        """Return True if is a test plan."""
        return self.valid is None

    @property
    def is_missing_triage(self):
        """Return True if object failed and has no issues linked."""
        return not self.valid and not self.issueoccurrence_set.exists()

    @classmethod
    def create_from_json(cls, data):
        """Create KCIDBBuild from kcidb json."""
        misc = data.get('misc', {})

        try:
            existing_build = KCIDBBuild.objects.get(id=data['id'])
        except KCIDBBuild.DoesNotExist:
            existing_build = None

        arch = pipeline_models.ArchitectureEnum[data['architecture']] if data.get('architecture') else None

        compiler = data.get('compiler')
        compiler = pipeline_models.Compiler.objects.get_or_create(name=compiler)[0] if compiler else None

        log = data.get('log_url')
        log = file_models.Artifact.create_from_url(log) if log else None

        try:
            checkout = KCIDBCheckout.objects.get(id=data['checkout_id'], origin__name=data['origin'])
        except KCIDBCheckout.DoesNotExist as exc:
            raise MissingParent(KCIDBCheckout, data['checkout_id']) from exc

        origin = KCIDBOrigin.create_from_string(data['origin'])
        build, _ = cls.objects.prefetch_related(
                # Prefetch to optimize serialization while sending the kcidb message
                Prefetch('checkout', queryset=KCIDBCheckout.objects.defer("patchset_modified_files")),
            ).update_or_create(
            checkout=checkout,
            id=data['id'],
            origin=origin,
            # for an empty data dict, all of these have to be None
            defaults=utils.clean_dict({
                'comment': data.get('comment'),
                'start_time': data.get('start_time'),
                'duration': data.get('duration'),
                'architecture': arch,
                'command': data.get('command'),
                'compiler': compiler,
                'config_name': data.get('config_name'),
                'config_url': data.get('config_url'),
                'log': log,
                'log_excerpt': data.get('log_excerpt'),
                'valid': data.get('valid'),
                'test_plan_missing': misc.get('test_plan_missing'),
                'debug': misc.get('debug'),
                'retrigger': misc.get('retrigger'),
                'kpet_tree_name': misc.get('kpet_tree_name'),
                'kpet_tree_family': misc.get('kpet_tree_family'),
                'package_name': misc.get('package_name'),
                'testing_skipped_reason': misc.get('testing_skipped_reason'),
            })
        )

        if input_files := data.get('input_files'):
            build.input_files.set(
                file_models.Artifact.create_from_json(input_file)
                for input_file in input_files
            )

        if output_files := data.get('output_files'):
            build.output_files.set(
                file_models.Artifact.create_from_json(output_file)
                for output_file in output_files
            )

        if provenance_components := misc.get('provenance'):
            build.provenance.set([
                ProvenanceComponent.create_from_dict(data)
                for data in provenance_components
            ])

        utils.created_object_send_message(existing_build, build, 'build')

        metrics.update_time_to_build.delay(build.iid)

        return build


class KCIDBTestManager(Manager):
    # pylint: disable=too-few-public-methods
    """Manager for KCIDBTestManager."""

    def filter_untriaged(self):
        """Filter untriaged tests."""
        return self.filter(
            status__in=KCIDBTest.UNSUCCESSFUL_STATUSES,
            issues=None
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBTest(EMOM('kcidb_test'), Model, ActionsDataModel):
    """Model for KCIDBTest."""

    iid = models.AutoField(primary_key=True)

    build = models.ForeignKey('KCIDBBuild', on_delete=models.CASCADE)
    id = models.CharField(max_length=200, unique=True)
    origin = models.ForeignKey(KCIDBOrigin, on_delete=models.PROTECT)
    environment = models.ForeignKey('BeakerResource', on_delete=models.PROTECT, null=True, blank=True)
    test = models.ForeignKey('Test', on_delete=models.PROTECT, null=True, blank=True)
    waived = models.BooleanField(null=True)
    start_time = models.DateTimeField(null=True, blank=True)
    duration = models.IntegerField(null=True, blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='test', blank=True)

    status = models.CharField(max_length=1, choices=test_models.ResultEnum.choices,
                              null=True, blank=True)

    log = models.ForeignKey('Artifact', on_delete=models.SET_NULL, null=True, blank=True)
    log_excerpt = models.TextField(null=True, blank=True)

    # Fields not belonging to KCIDB schema
    provenance = models.ManyToManyField(ProvenanceComponent, blank=True, related_name='tests')
    targeted = models.BooleanField(null=True, blank=True)
    retrigger = models.BooleanField(blank=True, null=True)

    objects = KCIDBTestManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'build__checkout__policy'

    UNSUCCESSFUL_STATUSES = (test_models.ResultEnum.ERROR, test_models.ResultEnum.FAIL)

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    @property
    def checkout(self):
        """
        Return the checkout for this object.

        Provide a transparent way of doing obj.checkout no matter which KCIDB object it is.
        """
        return self.build.checkout

    class Meta:
        """Metadata."""

        ordering = ('iid',)

    def is_equivalent_to(self, other):
        """Compare two KCIDBTest objects using a list of fields, otherwise False."""
        fields = ['id', 'status', 'waived', 'start_time', 'duration', 'log_excerpt', 'targeted']
        if isinstance(other, KCIDBTest):
            return utils.clean_dict(model_to_dict(self, fields=fields)) == \
                utils.clean_dict(model_to_dict(other, fields=fields))
        return False

    @property
    def web_url(self):
        """Return the URL to this object in the web interface."""
        return settings.DATAWAREHOUSE_URL + reverse('views.kcidb.tests', args=[self.iid])

    @property
    def is_public(self):
        """Return True if this KCIDBTest is public."""
        return self.build.checkout.kernel_type == KernelTypeEnum.UPSTREAM

    @property
    def is_test_plan(self):
        """Return True if is a test plan."""
        return self.status is None

    @property
    def is_missing_triage(self):
        """Return True if object failed and has no issues linked."""
        return self.status in self.UNSUCCESSFUL_STATUSES and not self.issueoccurrence_set.exists()

    @classmethod
    def create_from_json(cls, data):
        # pylint: disable=too-many-locals
        """Create KCIDBTest from kcidb json."""
        misc = data.get('misc', {})

        try:
            existing_test = KCIDBTest.objects.get(id=data['id'])
        except KCIDBTest.DoesNotExist:
            existing_test = None

        try:
            build = KCIDBBuild.objects.get(id=data['build_id'])
        except KCIDBBuild.DoesNotExist as exc:
            raise MissingParent(KCIDBBuild, data['build_id']) from exc

        environment = data.get('environment')
        environment = test_models.BeakerResource.objects.get_or_create(
            fqdn=environment['comment']
        )[0] if environment else None

        test_comment = data.get('comment')
        if test_comment:
            test = test_models.Test.get_and_update(
                name=test_comment,
                universal_id=data.get('path'),
                maintainers=misc.get('maintainers'),
                fetch_url=misc.get('fetch_url'),
            )
        else:
            test = None

        status = test_models.ResultEnum[data['status']] if data.get('status') else None

        log_url = data.get('log_url')
        log = file_models.Artifact.create_from_url(log_url) if log_url else None

        origin = KCIDBOrigin.create_from_string(data['origin'])
        test, created = cls.objects.prefetch_related(
                # Prefetch to optimize serialization while sending the kcidb message
                Prefetch('build__checkout', queryset=KCIDBCheckout.objects.defer("patchset_modified_files")),
            ).update_or_create(
            build=build,
            id=data['id'],
            origin=origin,
            # for an empty data dict, all of these have to be None
            defaults=utils.clean_dict({
                'environment': environment,
                'test': test,
                'status': status,
                'waived': data.get('waived'),
                'start_time': data.get('start_time'),
                'duration': data.get('duration'),
                'log': log,
                'log_excerpt': data.get('log_excerpt'),
                'retrigger': misc.get('retrigger'),
            })
        )

        if created:
            # Set default values, should not be overwritten when resubmitted without these fields
            test.targeted = misc.get('targeted', False)
            test.save()

        if output_files := data.get('output_files'):
            test.output_files.set(
                file_models.Artifact.create_from_json(output_file)
                for output_file in output_files
            )

        for result in misc.get('results', []):
            KCIDBTestResult.create_from_json(test, result)

        if provenance_components := misc.get('provenance'):
            test.provenance.set([
                ProvenanceComponent.create_from_dict(data)
                for data in provenance_components
            ])

        utils.created_object_send_message(existing_test, test, 'test')

        return test


class KCIDBTestResult(EMOM('kcidb_test_result'), ActionsDataModel):
    """Model for KCIDBTestResult."""

    iid = models.AutoField(primary_key=True)

    test = models.ForeignKey('KCIDBTest', on_delete=models.CASCADE)
    id = models.CharField(max_length=200, unique=True)
    name = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=1, choices=test_models.ResultEnum.choices,
                              null=True, blank=True)
    output_files = models.ManyToManyField('Artifact', related_name='test_result', blank=True)

    objects = KCIDBTestManager()

    # Link this object to the related one defining the authorization
    path_to_policy = 'test__build__checkout__policy'

    def __str__(self):
        """Return __str__ formatted."""
        return self.id

    class Meta:
        """Metadata."""

        ordering = ('iid',)

    @property
    def checkout(self):
        """
        Return the checkout for this object.

        Provide a transparent way of doing obj.checkout no matter which KCIDB object it is.
        """
        return self.test.checkout

    @classmethod
    def create_from_json(cls, test, data):
        # pylint: disable=too-many-locals
        """Create KCIDBTestResult from kcidb test result json."""
        status = test_models.ResultEnum[data['status']] if data.get('status') else None
        result, _ = cls.objects.update_or_create(
            test=test,
            id=data['id'],
            defaults=utils.clean_dict({
                'status': status,
                'name': data.get('name'),
            })
        )

        if output_files := data.get('output_files'):
            result.output_files.set(
                file_models.Artifact.create_from_json(output_file)
                for output_file in output_files
            )

        # KCIDBTestResult are not updated, so force existing_object=None
        utils.created_object_send_message(None, result, 'testresult')

        return result
