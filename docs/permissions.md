# Permissions

The [Data Warehouse] uses Django [Auth Module] with Users, Groups and
Permissions to limit access.

All the read operations (GET Requests) are allowed without authentication.
Write permissions (POST, PUT and DELETE) require specific permissions.

To read more about how Django's permissions work, check the [Auth Module]
documentation.

## Creating an Account

Accounts are currently managed by us. If you want an account, you need to ask
for one on the #team-kernel-cki Slack channel.

## CKI Team

The Data Warehouse contains a [Group] called 'CKI Team' which has all the write
permissions necessary. When granting access to users, adding them to the 'CKI
Team' is the recommended way to proceed.

## Consuming the API

API Endpoints are also managed with this configuration. GET requests
work unauthenticated, but you need to authenticate to do POST, DELETE,
and other types of requests. See [API auth] documentation for more
information.

### Requesting API tokens

To request an API Token, please ask on the #team-kernel-cki Slack channel.

Then, anyone that has admin permissions (that means _both_ the
`is_superuser` and the `is_staff` flags set in the user record in the
`auth_user` table) can go to the Django [admin interface] and create
one for you by going to the "Tokens" collection under "Auth token" and
clicking on "Add token" on the top right.

[Data Warehouse]: https://datawarehouse.cki-project.org/
[Auth Module]: https://docs.djangoproject.com/en/3.0/topics/auth/default
[Group]: https://docs.djangoproject.com/en/3.0/topics/auth/default/#groups
[API auth]: /api/auth.html
[admin interface]: https://datawarehouse.cki-project.org/admin/
