# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to the files."""
import datetime
from functools import lru_cache
import pathlib
from urllib.parse import urlparse

from django.conf import settings
from django.db import models
from django.utils import timezone
from django_prometheus.models import ExportModelOperationsMixin as EMOM


class Artifact(EMOM('artifact'), models.Model):
    """Model for Artifact."""

    name = models.CharField(max_length=150)
    url = models.URLField(max_length=400)
    valid_for = models.PositiveSmallIntegerField(null=True)
    expiry_date = models.DateTimeField(null=True)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name}'

    class Meta:
        """Metadata."""

        unique_together = ('name', 'url')

    @staticmethod
    @lru_cache
    # lru_cache is ok here as static/class methods don't suffer from the issues described at
    # https://rednafi.github.io/reflections/dont-wrap-instance-methods-with-functoolslru_cache-decorator-in-python.html
    def _get_valid_for(url):
        """
        Get appropiate valid_for value.

        Try to match url with the ones configured in ARTIFACTS_KNOWN_VALID_FOR,
        return ARTIFACTS_DEFAULT_VALID_FOR_DAYS if not found.
        """
        return int(
            settings.ARTIFACTS_KNOWN_VALID_FOR.get(
                url,
                settings.ARTIFACTS_DEFAULT_VALID_FOR_DAYS
            )
        )

    @staticmethod
    def _name(url):
        return pathlib.Path(urlparse(url).path).name

    def save(self, *args, **kwargs):
        # pylint: disable=signature-differs
        """
        Override save method.

        Generate name from url if not provided.
        """
        parsed_url = urlparse(self.url)

        if not self.name:
            self.name = self._name(self.url)

        if not self.valid_for:
            # Call _get_valid_for with the bucket url (without file path specifics)
            bucket = f"{parsed_url.netloc}/{parsed_url.path[1:].split('/')[0]}"
            self.valid_for = self._get_valid_for(bucket)

        # When created, add an expiry_date if valid_for
        if not self.expiry_date:
            self.expiry_date = (
                timezone.now() + datetime.timedelta(days=self.valid_for)
            )

        super().save(*args, **kwargs)

    @classmethod
    def create_from_url(cls, url):
        """Create Artifact from url."""
        return cls.objects.get_or_create(url=url, name=cls._name(url))[0]

    @classmethod
    def create_from_json(cls, data):
        """Create Artifact from KCIDB JSON format."""
        return cls.objects.get_or_create(
            url=data['url'],
            name=data['name'],
        )[0]
