"""Test the task_models module."""
import datetime
from unittest import mock

from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import models
from tests import utils


class QueuedTaskTest(utils.TestCase):
    """Unit tests for the QueuedTask model."""

    @freeze_time("2010-01-02 09:00:00")
    def test_create_new(self):
        """Test creating a new QueuedTask."""
        models.QueuedTask.create(
            name='do_foo',
            call_id='test_task',
            call_kwargs={'foo': 'bar'}
        )

        task = models.QueuedTask.objects.get()
        self.assertEqual('do_foo', task.name)
        self.assertEqual('test_task', task.call_id)
        self.assertEqual([{'foo': 'bar'}], task.calls_kwargs)
        self.assertEqual(datetime.datetime(2010, 1, 2, 9, 5, tzinfo=datetime.timezone.utc),
                         task.run_at)

    def test_create_already_existing(self):
        """Test creating a QueuedTask when the task already exists."""
        with freeze_time("2010-01-02 09:00:00"):
            models.QueuedTask.create(
                name='do_foo',
                call_id='test_task',
                call_kwargs={'foo': 'bar'}
            )

        task = models.QueuedTask.objects.get()
        self.assertEqual(datetime.datetime(2010, 1, 2, 9, 5, tzinfo=datetime.timezone.utc),
                         task.run_at)

        with freeze_time("2010-01-02 09:02:00"):
            models.QueuedTask.create(
                name='do_foo',
                call_id='test_task',
                call_kwargs={'foo': 'baz'}
            )

        task.refresh_from_db()
        self.assertEqual([{'foo': 'bar'}, {'foo': 'baz'}], task.calls_kwargs)
        self.assertEqual(datetime.datetime(2010, 1, 2, 9, 7, tzinfo=datetime.timezone.utc),
                         task.run_at)

    def test_create_same_task_different_id(self):
        """Test creating QueuedTask with different ids."""
        self.assertFalse(models.QueuedTask.objects.exists())

        models.QueuedTask.create(name='task_1', call_id='task_1_1', call_kwargs=None)
        models.QueuedTask.create(name='task_1', call_id='task_1_1', call_kwargs=None)

        self.assertEqual(1, models.QueuedTask.objects.count())

        # Different name, same call_id
        models.QueuedTask.create(name='task_2', call_id='task_1_1', call_kwargs=None)
        # Same name, different call_id
        models.QueuedTask.create(name='task_1', call_id='task_1_2', call_kwargs=None)

        self.assertEqual(3, models.QueuedTask.objects.count())

    @mock.patch('datawarehouse.models.task_models.celery.app.signature')
    def test_run(self, mock_celery):
        """Test running a QueuedTask."""
        function = mock.Mock()
        mock_celery.return_value = function
        task = models.QueuedTask.create(
            name='task_1',
            call_id='task_1_1',
            call_kwargs={'foo': 'bar'}
        )
        task.run()

        function.assert_called_with(task.calls_kwargs)


class QueuedTaskManagerTest(utils.TestCase):
    """Unit tests for the QueuedTaskManager."""

    def test_filter_ready_to_run(self):
        """Test getting ready to run tasks."""
        def create_task(name, run_in):
            models.QueuedTask.objects.create(
                name=name,
                call_id=name,
                calls_kwargs=[],
                run_at=timezone.now() + datetime.timedelta(minutes=run_in)
            )

        tasks = [
            ('task_1', 1),
            ('task_2', 2),
            ('task_3', 3),
            ('task_4', 4),
            ('task_5', 5),
        ]

        with freeze_time("2010-01-02 09:00:00"):
            for name, run_in in tasks:
                create_task(name, run_in)

            self.assertEqual(
                [],
                [t.name for t in models.QueuedTask.objects.filter_ready_to_run()]
            )

        with freeze_time("2010-01-02 09:02:00"):
            self.assertEqual(
                ['task_1', 'task_2'],
                [t.name for t in models.QueuedTask.objects.filter_ready_to_run()]
            )

        with freeze_time("2010-01-02 09:04:00"):
            self.assertEqual(
                ['task_1', 'task_2', 'task_3', 'task_4'],
                [t.name for t in models.QueuedTask.objects.filter_ready_to_run()]
            )

        with freeze_time("2010-01-02 09:10:00"):
            self.assertEqual(
                ['task_1', 'task_2', 'task_3', 'task_4', 'task_5'],
                [t.name for t in models.QueuedTask.objects.filter_ready_to_run()]
            )
