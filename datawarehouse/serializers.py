# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Serializers."""
from django.db.models import Max
from django.db.models import Min
from rest_framework import serializers

from . import models


# Misc
class GitTreeSerializer(serializers.ModelSerializer):
    """Serializer for GitTree model."""

    class Meta:
        """Metadata."""

        model = models.GitTree
        fields = ('id', 'name',)


class PolicySerializer(serializers.ModelSerializer):
    """Serializer for Policy model."""

    class Meta:
        """Metadata."""

        model = models.Policy
        fields = ('id', 'name')


# Merge
class PatchSerializer(serializers.ModelSerializer):
    """Serializer for Patch model."""

    class Meta:
        """Metadata."""

        model = models.Patch
        fields = ('url', 'subject')


# Build
class CompilerSerializer(serializers.ModelSerializer):
    """Serializer for Compiler model."""

    class Meta:
        """Metadata."""

        model = models.Compiler
        fields = ('name',)


# Test
class TestMaintainerSerializer(serializers.ModelSerializer):
    """Serializer for TestMaintainer model."""

    class Meta:
        """Metadata."""

        model = models.TestMaintainer
        fields = ('name', 'email', 'gitlab_username')


class TestSerializer(serializers.ModelSerializer):
    """Serializer for Test model."""

    maintainers = TestMaintainerSerializer(many=True)

    class Meta:
        """Metadata."""

        model = models.Test
        fields = ('id', 'name', 'fetch_url', 'maintainers', 'universal_id',)


class TestSimpleSerializer(TestSerializer):
    """Serializer for Test model. Simple."""

    class Meta(TestSerializer.Meta):
        """Metadata."""

        fields = ('id', 'name', 'fetch_url')


class TestConfidenceSerializer(TestSerializer):
    """Serializer for Test model. With confidence metrics."""

    class Meta(TestSerializer.Meta):
        """Metadata."""

        fields = TestSerializer.Meta.fields + ('confidence',)


class ArtifactSerializer(serializers.ModelSerializer):
    """Serializer for Artifact model."""

    # Temporarily keep file support to make the change on the lib.
    file = serializers.CharField(source='url')

    class Meta:
        """Metadata."""

        model = models.Artifact
        fields = ('name', 'file', 'url')


class BeakerResourceSerializer(serializers.ModelSerializer):
    """Serializer for BeakerResource model."""

    class Meta:
        """Metadata."""

        model = models.BeakerResource
        fields = ('id', 'fqdn',)


class BeakerResourceConfidenceSerializer(BeakerResourceSerializer):
    """Serializer for BeakerResource model. With confidence metrics."""

    class Meta(BeakerResourceSerializer.Meta):
        """Metadata."""

        fields = BeakerResourceSerializer.Meta.fields + ('confidence',)


class RecipientSerializer(serializers.ModelSerializer):
    """Serializer for Recipient model."""

    class Meta:
        """Metadata."""

        model = models.Recipient
        fields = ("email",)


class ReportSerializer(serializers.ModelSerializer):
    """Serializer for Report model."""

    addr_to = RecipientSerializer(many=True)
    addr_cc = RecipientSerializer(many=True)

    class Meta:
        """Metadata."""

        model = models.Report
        fields = "__all__"


class IssueKindSerializer(serializers.ModelSerializer):
    """Serializer for IssueKind model."""

    class Meta:
        """Metadata."""

        model = models.IssueKind
        fields = ('id', 'description', 'tag',)


class IssueSerializer(serializers.ModelSerializer):
    """Serializer for Issue model."""

    kind = IssueKindSerializer()
    policy = PolicySerializer()
    first_seen = serializers.SerializerMethodField()
    last_seen = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.Issue
        fields = ('id', 'kind', 'description', 'ticket_url', 'resolved', 'resolved_at', 'policy',
                  'first_seen', 'last_seen')

    @staticmethod
    def _timestamps(issue):
        # pylint: disable=no-self-use
        """Return timestamp of the first and last checkouts where this issue was seen."""
        return (
            # Don't use issue.get_checkouts() as we don't care about the
            # authorization filter, it's just the timestamp.
            issue.issueoccurrence_set
            .aggregate(
                first_occurrence=Min('related_checkout__start_time'),
                last_occurrence=Max('related_checkout__start_time')
            )
        )

    def get_first_seen(self, issue):
        """Return timestamp of the first checkout where this issue was seen."""
        return self._timestamps(issue)['first_occurrence']

    def get_last_seen(self, issue):
        """Return timestamp of the last checkout where this issue was seen."""
        return self._timestamps(issue)['last_occurrence']


class IssueRegexSerializer(serializers.ModelSerializer):
    """Serializer for IssueRegex model."""

    issue = IssueSerializer()

    class Meta:
        """Metadata."""

        model = models.IssueRegex
        fields = ('id', 'issue', 'text_match', 'file_name_match', 'test_name_match',
                  'architecture_match', 'tree_match', 'kpet_tree_name_match', 'package_name_match')


class IssueOccurrenceSerializer(serializers.ModelSerializer):
    """Serializer for IssueOccurrence model."""

    issue = IssueSerializer()

    class Meta:
        """Metadata."""

        model = models.IssueOccurrence
        fields = ('issue', 'is_regression', 'created_at')


class IssueLightSerializer(serializers.ModelSerializer):
    """Serializer for Issue model with only lightweight keys."""

    kind = IssueKindSerializer()
    policy = PolicySerializer()

    class Meta:
        """Metadata."""

        model = models.Issue
        fields = ('id', 'kind', 'description', 'ticket_url', 'resolved', 'resolved_at', 'policy')


class IssueOccurrenceLightSerializer(serializers.ModelSerializer):
    """Serializer for IssueOccurrence model with only lightweight keys."""

    issue = IssueLightSerializer()
    checkout_id = serializers.CharField(source='kcidb_checkout.id', default=None)
    build_id = serializers.CharField(source='kcidb_build.id', default=None)
    test_id = serializers.CharField(source='kcidb_test.id', default=None)
    testresult_id = serializers.CharField(source='kcidb_testresult.id', default=None)

    class Meta:
        """Metadata."""

        model = models.IssueOccurrence
        fields = ('issue', 'is_regression', 'created_at', 'checkout_id', 'build_id', 'test_id', 'testresult_id')
