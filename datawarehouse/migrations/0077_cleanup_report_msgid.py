"""Cleanup reports to remove < > from msgid."""
from email.utils import parseaddr

from django.db import migrations, models


def clean_msgids(apps, schema_editor):
    """Cleanup Reports Message ID."""
    Report = apps.get_model('datawarehouse', 'Report')
    db_alias = schema_editor.connection.alias

    reports = (
        Report.objects
        .using(db_alias)
        .filter(msgid__startswith='<')
        .iterator()
    )

    for report in reports:
        report.msgid = parseaddr(report.msgid)[1]
        report.save()


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0076_auto_20211025_1048'),
    ]

    operations = [
        migrations.RunPython(clean_msgids),
    ]
