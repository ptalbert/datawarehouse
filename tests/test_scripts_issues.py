"""Test scripts/issues.py."""
import datetime
from unittest import mock

import django.contrib.auth.models as auth_models
from django.utils import timezone

from datawarehouse import models
from datawarehouse import scripts
from datawarehouse.scripts import issues
from datawarehouse.utils import datetime_bool
from tests import utils


class TestUpdateIssueOccurrencesRelatedCheckout(utils.TestCase):
    """
    Test update_issue_occurrences_related_checkout.

    Ensure IssueOccurrence.related_checkout is populated after
    assigning an issue to a KCIDB object.
    """

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up test."""
        self.issue = models.Issue.objects.last()

    def test_checkout(self):
        """"Test update_issue_occurrences_related_checkout with checkout."""
        checkout = models.KCIDBCheckout.objects.last()

        # Create IssueOccurrence to avoid triggering the signal
        models.IssueOccurrence.objects.create(issue=self.issue, kcidb_checkout=checkout)
        scripts.update_issue_occurrences_related_checkout(checkout, (self.issue.id, ))

        issue_occurrence = models.IssueOccurrence.objects.last()
        self.assertEqual(issue_occurrence.kcidb_checkout, checkout)
        self.assertEqual(issue_occurrence.related_checkout, checkout)

    def test_build(self):
        """"Test update_issue_occurrences_related_checkout with build."""
        build = models.KCIDBBuild.objects.last()

        # Create IssueOccurrence to avoid triggering the signal
        models.IssueOccurrence.objects.create(issue=self.issue, kcidb_build=build)
        scripts.update_issue_occurrences_related_checkout(build, (self.issue.id, ))

        issue_occurrence = models.IssueOccurrence.objects.last()
        self.assertEqual(issue_occurrence.kcidb_build, build)
        self.assertEqual(issue_occurrence.related_checkout, build.checkout)

    def test_test(self):
        """"Test update_issue_occurrences_related_checkout with test."""
        test = models.KCIDBTest.objects.last()

        # Create IssueOccurrence to avoid triggering the signal
        models.IssueOccurrence.objects.create(issue=self.issue, kcidb_test=test)
        scripts.update_issue_occurrences_related_checkout(test, (self.issue.id, ))

        issue_occurrence = models.IssueOccurrence.objects.last()
        self.assertEqual(issue_occurrence.kcidb_test, test)
        self.assertEqual(issue_occurrence.related_checkout, test.build.checkout)

    def test_multiple(self):
        """"Test update_issue_occurrences_related_checkout when adding multiple objects."""
        test = models.KCIDBTest.objects.last()
        issues = models.Issue.objects.all()

        # Create IssueOccurrence to avoid triggering the signal
        for issue in issues:
            models.IssueOccurrence.objects.create(issue=issue, kcidb_test=test)
        scripts.update_issue_occurrences_related_checkout(test, (i.id for i in issues))

        for issue_occurrence in models.IssueOccurrence.objects.all():
            self.assertEqual(issue_occurrence.kcidb_test, test)
            self.assertEqual(issue_occurrence.related_checkout, test.build.checkout)


class TestUpdateIssueOccurrencesRegression(utils.TestCase):
    """Test update_issue_occurrences_regression."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up test."""
        self.issue = models.Issue.objects.last()

    @staticmethod
    def _test_objects():
        objs = [
            (models.KCIDBCheckout.objects.last(), 'kcidb_checkout'),
            (models.KCIDBBuild.objects.last(), 'kcidb_build'),
            (models.KCIDBTest.objects.last(), 'kcidb_test'),
            (models.KCIDBTestResult.objects.last(), 'kcidb_testresult'),
        ]

        for obj, key in objs:
            if key != 'kcidb_testresult':
                obj.start_time = timezone.now()
                obj.save()

            yield key, obj

    def test_issue_not_resolved(self):
        """"Test when the issue is not resolved."""
        for key, obj in self._test_objects():
            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=self.issue, **{key: obj})
            scripts.update_issue_occurrences_regression(obj, (self.issue.id, ))

            issue_occurrence = models.IssueOccurrence.objects.last()
            self.assertFalse(issue_occurrence.is_regression)

    def test_issue_resolved_after(self):
        """"Test when the issue was resolved after the object run."""
        for key, obj in self._test_objects():
            self.issue.resolved_at = (
                (obj.test.start_time if isinstance(obj, models.KCIDBTestResult) else obj.start_time)
                + datetime.timedelta(seconds=1)
            )
            self.issue.save()

            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=self.issue, **{key: obj})
            scripts.update_issue_occurrences_regression(obj, (self.issue.id, ))

            issue_occurrence = models.IssueOccurrence.objects.last()
            self.assertFalse(issue_occurrence.is_regression)

    def test_issue_resolved_before(self):
        """"Test when the issue was resolved before the object run."""
        for key, obj in self._test_objects():
            self.issue.resolved_at = (
                (obj.test.start_time if isinstance(obj, models.KCIDBTestResult) else obj.start_time)
                + datetime.timedelta(seconds=-1)
            )
            self.issue.save()

            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=self.issue, **{key: obj})
            scripts.update_issue_occurrences_regression(obj, (self.issue.id, ))

            issue_occurrence = models.IssueOccurrence.objects.last()
            self.assertTrue(issue_occurrence.is_regression)

    def test_issue_resolved_before_no_start_time(self):
        """"Test when the issue was resolved before the object run but it has no start_time."""
        for key, obj in self._test_objects():
            obj.start_time = None
            obj.save()

            self.issue.resolved_at = timezone.now()
            self.issue.save()

            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=self.issue, **{key: obj})
            scripts.update_issue_occurrences_regression(obj, (self.issue.id, ))

            issue_occurrence = models.IssueOccurrence.objects.last()
            self.assertTrue(issue_occurrence.is_regression)


class TestSendRegressionNotification(utils.TestCase):
    """Test send_regression_notification."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/issues.yaml',
    ]

    @staticmethod
    def _test_objects():
        objs = [
            (models.KCIDBCheckout.objects.last(), 'kcidb_checkout'),
            (models.KCIDBBuild.objects.last(), 'kcidb_build'),
            (models.KCIDBTest.objects.last(), 'kcidb_test'),
            (models.KCIDBTestResult.objects.last(), 'kcidb_testresult'),
        ]

        for obj, key in objs:
            if key != 'kcidb_testresult':
                obj.start_time = timezone.now()
                obj.save()

            yield key, obj

    @mock.patch('datawarehouse.scripts.issues.settings.FF_NOTIFY_ISSUE_REGRESSION', True)
    @mock.patch('datawarehouse.scripts.issues.notify_issue_regression')
    def test_sends_only_regressions(self, mock_notify):
        """"Test it only sends notifications for regressions."""
        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)

        for key, obj in self._test_objects():
            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=issue_1, **{key: obj}, is_regression=True)
            models.IssueOccurrence.objects.create(issue=issue_2, **{key: obj}, is_regression=False)
            scripts.send_regression_notification(obj, (issue_1.id, issue_2.id))

            self.assertEqual(1, mock_notify.call_count)
            if isinstance(obj, models.KCIDBTestResult):
                mock_notify.assert_has_calls([mock.call(obj.test, issue_1)])
            else:
                mock_notify.assert_has_calls([mock.call(obj, issue_1)])
            mock_notify.reset_mock()

    @mock.patch('datawarehouse.scripts.issues.settings.FF_NOTIFY_ISSUE_REGRESSION', False)
    @mock.patch('datawarehouse.scripts.issues.notify_issue_regression')
    def test_verify_issue_regression_disable(self, mock_notify):
        """Test verify_issue_regression method. FF_NOTIFY_ISSUE_REGRESSION is disabled."""
        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)

        for key, obj in self._test_objects():
            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=issue_1, **{key: obj}, is_regression=True)
            models.IssueOccurrence.objects.create(issue=issue_2, **{key: obj}, is_regression=False)
            scripts.send_regression_notification(obj, (issue_1.id, issue_2.id))

            self.assertFalse(mock_notify.called)

    @mock.patch('datawarehouse.scripts.issues.settings.FF_NOTIFY_ISSUE_REGRESSION', True)
    @mock.patch('datawarehouse.scripts.issues.notify_issue_regression')
    def test_verify_issue_regression_retrigger(self, mock_notify):
        """Test verify_issue_regression method. Checkout is retriggered."""
        issue_1 = models.Issue.objects.get(id=1)

        for key, obj in self._test_objects():
            obj.checkout.retrigger = True
            obj.checkout.save()

            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=issue_1, **{key: obj}, is_regression=True)
            scripts.send_regression_notification(obj, (issue_1.id, ))

            self.assertFalse(mock_notify.called)


class TestRegressionNotifications(utils.TestCase):
    """Unit tests for issue_regression methods."""

    fixtures = [
        'tests/fixtures/issue_regressions.yaml',
    ]

    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_notify_issue_regression_subscribers(self, mock_send_mail):
        """Test notify_issue_regression method for subscribed users."""
        checkout = models.KCIDBCheckout.objects.get(id='checkout_1')
        issue = models.Issue.objects.get(description='issue_1')

        issues.notify_issue_regression(checkout, issue)

        # No user is subscribed
        self.assertFalse(mock_send_mail.called)

        # Subscribe all users to issue regressions
        for user in auth_models.User.objects.all():
            models.UserSubscriptions.objects.get_or_create(
                user=user,
                issue_regression_subscribed_at=datetime_bool(True),
                issue_regression_visibility=models.SubscriptionVisibility.CC
            )

        issues.notify_issue_regression(checkout, issue)

        users = auth_models.User.objects.filter(username__in=('user_4', 'user_5'))
        subject = f'{ issue.kind.tag } | Issue #{ issue.id} regression detected'
        message = (
            'Hello,\n\n'
            'You are receiving this email because a regression was detected\n'
            'and tagged in DataWarehouse.\n\n'
            f'Issue: { issue.description}\n'
            f'URL: { issue.web_url }\n'
            f'Seen in: { checkout.web_url }\n\n'
            '--\n'
            'The DataWarehouse team.\n'
        )

        mock_send_mail.assert_called_with(subject, message, mock.ANY)
        self.assertEqual(
            set(mock_send_mail.call_args_list[0][0][2]['cc']),
            set(u.email for u in users)
        )

    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_notify_issue_regression_subscribers_cc_bcc(self, mock_send_mail):
        """Test notify_issue_regression method for subscribed users respects visibility."""
        checkout = models.KCIDBCheckout.objects.get(id='checkout_1')
        issue = models.Issue.objects.get(description='issue_1')

        models.UserSubscriptions.objects.create(
            user=auth_models.User.objects.get(username='user_4'),
            issue_regression_subscribed_at=datetime_bool(True),
            issue_regression_visibility=models.SubscriptionVisibility.CC
        )

        models.UserSubscriptions.objects.create(
            user=auth_models.User.objects.get(username='user_5'),
            issue_regression_subscribed_at=datetime_bool(True),
            issue_regression_visibility=models.SubscriptionVisibility.BCC
        )

        issues.notify_issue_regression(checkout, issue)

        mock_send_mail.assert_called_with(
            mock.ANY, mock.ANY,
            {'cc': [auth_models.User.objects.get(username='user_4').email],
             'bcc': [auth_models.User.objects.get(username='user_5').email]}
        )

    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_notify_issue_regression_maintainers(self, mock_send_mail):
        """Test notify_issue_regression method for test maintainers."""
        test = models.KCIDBTest.objects.get(id='test_1')
        issue = models.Issue.objects.get(description='issue_1')

        issues.notify_issue_regression(test, issue)

        subject = f'{ issue.kind.tag } | Issue #{ issue.id} regression detected'
        message = (
            'Hello,\n\n'
            'You are receiving this email because a regression was detected\n'
            'and tagged in DataWarehouse.\n\n'
            f'Issue: { issue.description}\n'
            f'URL: { issue.web_url }\n'
            f'Seen in: { test.web_url }\n\n'
            '--\n'
            'The DataWarehouse team.\n'
        )

        mock_send_mail.assert_called_with(
            subject, message, {'to': [test.test.maintainers.first().email], 'cc': [], 'bcc': []}
        )

    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_notify_issue_regression_maintainers_scratch(self, mock_send_mail):
        """
        Test notify_issue_regression method for scratch checkouts.

        On scratch checkouts, only the submitter should be notified.
        """
        test = models.KCIDBTest.objects.get(id='test_1')
        issue = models.Issue.objects.get(description='issue_1')

        test.build.checkout.scratch = True
        test.build.checkout.submitter = models.Maintainer.objects.create(email='co-submitter@mail.com')
        test.build.checkout.save()

        models.UserSubscriptions.objects.create(
            user=auth_models.User.objects.get(username='user_4'),
            issue_regression_subscribed_at=datetime_bool(True),
            issue_regression_visibility=models.SubscriptionVisibility.CC
        )

        issues.notify_issue_regression(test, issue)

        # Notification is only sent to the submitter, not to the
        # test maintainer.
        mock_send_mail.assert_called_with(
            mock.ANY, mock.ANY,
            {'to': ['co-submitter@mail.com']}
        )
