"""Receiver functions for signals."""
from django import dispatch
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db.models.signals import m2m_changed
from django.db.models.signals import post_save
from django.utils import timezone

from datawarehouse import models
from datawarehouse import scripts
from datawarehouse import signals
from datawarehouse import utils
from datawarehouse.api.kcidb import serializers as kcidb_serializers

User = get_user_model()


@dispatch.receiver(signals.kcidb_object)
def send_kcidb_object_message(**kwargs):
    """Send new kcidb object message."""
    # NOTE: prefetching related models is advised to reduce extra queries while serializing
    serializers = {
        'checkout': kcidb_serializers.KCIDBCheckoutSerializer,
        'build': kcidb_serializers.KCIDBBuildSerializer,
        'test': kcidb_serializers.KCIDBTestSerializer,
        'testresult': kcidb_serializers.KCIDBTestResultSerializer,
    }

    status = kwargs['status']
    object_type = kwargs['object_type']
    objects = kwargs['objects']
    misc = kwargs.get('misc', {})

    messages = [{
        'timestamp': timezone.now().isoformat(),
        'status': status,
        'object_type': object_type,
        'object': serializers[object_type](object_instance).data,
        'id': object_instance.id,
        'iid': object_instance.iid,
        'misc': misc,
    } for object_instance in objects]
    utils.send_kcidb_notification(messages)


@dispatch.receiver(post_save, sender=models.IssueRegex)
def send_kcidb_objects_retriage(instance, **_):
    """Send kcidb objects for retriage after regexes are added / modified."""
    if settings.RABBITMQ_CONFIGURED:
        models.QueuedTask.create(
            name='datawarehouse.scripts.misc.send_kcidb_object_for_retriage',
            call_id='send_kcidb_object_for_retriage',
            call_kwargs={
                'since_days_ago': 15,
                'issueregex_id': instance.id,
            },
            run_in_minutes=settings.TIMER_RETRIAGE_PERIOD_S / 60
        )


@dispatch.receiver(m2m_changed, sender=models.KCIDBCheckout.issues.through)
@dispatch.receiver(m2m_changed, sender=models.KCIDBBuild.issues.through)
@dispatch.receiver(m2m_changed, sender=models.KCIDBTest.issues.through)
def issue_occurrence_assigned(action, sender, instance, pk_set, **_):
    """Signal handler to populate extra fields in IssueOccurrence."""
    # Only process post_add for issue_occurrence m2m.
    if action != 'post_add' or sender != models.IssueOccurrence:
        return

    scripts.update_issue_occurrences_related_checkout(instance, pk_set)
    scripts.update_issue_policy(pk_set)
    scripts.update_issue_occurrences_regression(instance, pk_set)
    scripts.send_regression_notification(instance, pk_set)


@dispatch.receiver(post_save, sender=User)
def sync_user_ldap_groups(created, instance, **_):
    """Trigger LDAP groups sync after user creation."""
    if created:
        scripts.update_ldap_group_members_for_user(instance)
