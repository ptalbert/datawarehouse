"""Django settings for datawarehouse project."""
import importlib.util
import os
from socket import gethostbyname
from socket import gethostname
from urllib.parse import urlparse

from cki_lib import misc
from debug_toolbar import settings as debug_toolbar_settings
import saml2
import saml2.saml
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
import yaml

# URL of the instance
DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL', '').rstrip('/')
DATAWAREHOUSE_DOMAIN_NAME = urlparse(DATAWAREHOUSE_URL).netloc

#################################################################################
#                                                                               #
# Django configurations                                                         #
#                                                                               #
#################################################################################

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SECRET_KEY = os.getenv('SECRET_KEY')

IS_PRODUCTION_OR_STAGING = misc.is_production_or_staging()
CSRF_COOKIE_SECURE = IS_PRODUCTION_OR_STAGING
CORS_ORIGIN_ALLOW_ALL = IS_PRODUCTION_OR_STAGING
DEBUG = misc.get_env_bool("DEBUG", False) and not IS_PRODUCTION_OR_STAGING
SESSION_COOKIE_SECURE = IS_PRODUCTION_OR_STAGING

REQUESTS_MAX_WORKERS = os.environ.get("REQUESTS_MAX_WORKERS", 10)
REQUESTS_MAX_RETRIES = os.environ.get("REQUESTS_MAX_RETRIES", 3)
REQUESTS_TIMEOUT_S = os.environ.get("REQUESTS_TIMEOUT_S", 60)
REQUESTS_TIME_BETWEEN_RETRIES_S = os.environ.get("REQUESTS_TIME_BETWEEN_RETRIES_S", 2)  # noqa

ALLOWED_HOSTS = [
    DATAWAREHOUSE_DOMAIN_NAME or '*',
    gethostname(),
    gethostbyname(gethostname()),
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'datawarehouse',
    'rest_framework',
    'rest_framework.authtoken',
    'corsheaders',
    # 'debug_toolbar', #  conditionally added below
    'django_extensions',
    'django_prometheus',
    'captcha',
]

MIDDLEWARE = [
    'django_prometheus.middleware.PrometheusBeforeMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware', #  conditionally added below
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'datawarehouse.authorization.RequestAuthorization',
    'django_prometheus.middleware.PrometheusAfterMiddleware',
]

ROOT_URLCONF = 'datawarehouse.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # 'django.template.context_processors.debug', #  conditionally added below
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'datawarehouse.context_processors.settings_values',
                'datawarehouse.context_processors.enum_values',
            ],
        },
    },
]

WSGI_APPLICATION = 'datawarehouse.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django_prometheus.db.backends.postgresql',
        'NAME': os.environ.get('DB_NAME', 'datawarehouse'),
        'USER': os.environ.get('DB_USER', 'datawarehouse'),
        'PASSWORD': os.environ.get('DB_PASSWORD'),
        'HOST': os.environ.get('DB_HOST'),
        'PORT': misc.get_env_int('DB_PORT', 5432),
    }
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',  # noqa
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

STATIC_URL = '/static/'
STATIC_ROOT = '/code/static'

# ManifestStaticFilesStorage requires DEBUG=False and collectstatic to have run
if not DEBUG and IS_PRODUCTION_OR_STAGING:
    STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

MEDIA_PATH = '/staticfiles/'
MEDIA_URL = f'{DATAWAREHOUSE_URL}{MEDIA_PATH}'
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'staticfiles')

LOGIN_URL = '/user/-/login/'
LOGIN_REDIRECT_URL = '/dashboard'
LOGOUT_REDIRECT_URL = '/dashboard'

#################################################################################
#                                                                               #
# Sentry configuration                                                          #
#                                                                               #
#################################################################################
misc.sentry_init(sentry_sdk, integrations=[DjangoIntegration()])

#################################################################################
#                                                                               #
# Debug toolbar configuration                                                   #
#                                                                               #
#################################################################################


def show_toolbar(request):
    """Show the toolbar when requested with the debug parameter."""
    return DEBUG and (
        request.GET.get('debug') or request.path.startswith('/__debug__/')
    )


DEBUG_TOOLBAR_PANELS = debug_toolbar_settings.PANELS_DEFAULTS
DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'datawarehouse.settings.show_toolbar',
    'DISABLE_PANELS': [
        "debug_toolbar.panels.profiling.ProfilingPanel",
        "debug_toolbar.panels.redirects.RedirectsPanel",
    ]
}


if DEBUG:
    print("Debug toolbar is enabled!")
    INSTALLED_APPS.append('debug_toolbar')

    # Memory profiling
    if not importlib.util.find_spec('pympler'):
        print("Memory profiling not available. Missing required dependency: 'pympler'")
    else:
        print("Memory profiling is enabled!")
        INSTALLED_APPS.append('pympler')
        DEBUG_TOOLBAR_PANELS.append('pympler.panels.MemoryPanel')
        DEBUG_TOOLBAR_CONFIG['DISABLE_PANELS'].append("pympler.panels.MemoryPanel")

    MIDDLEWARE.insert(1, 'debug_toolbar.middleware.DebugToolbarMiddleware')
    for template in TEMPLATES:
        template['OPTIONS']['context_processors'].insert(0, 'django.template.context_processors.debug')


#################################################################################
#                                                                               #
# REST Framework configuration                                                  #
#                                                                               #
#################################################################################

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 30,
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],
    'DEFAULT_PARSER_CLASSES': [
        'rest_framework.parsers.JSONParser',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'datawarehouse.api.permissions.DjangoModelPermissionOrReadOnly',
    ]
}

#################################################################################
#                                                                               #
# Shell Plus configuration                                                      #
#                                                                               #
#################################################################################

SHELL_PLUS_PRINT_SQL_TRUNCATE = 10000
SHELL_PLUS_PRE_IMPORTS = (
    ('datawarehouse', 'models'),
)


#################################################################################
#                                                                               #
# RabbitMQ messaging server configuration                                       #
#                                                                               #
#################################################################################

RABBITMQ_HOST = os.environ.get('RABBITMQ_HOST', '').rstrip('/')
RABBITMQ_PORT = misc.get_env_int('RABBITMQ_PORT', 5672)
RABBITMQ_USER = os.environ.get('RABBITMQ_USER', 'guest')
RABBITMQ_PASSWORD = os.environ.get('RABBITMQ_PASSWORD', 'guest')
RABBITMQ_EXCHANGE_KCIDB_NOTIFICATIONS = os.environ.get('DATAWAREHOUSE_EXCHANGE_KCIDB',
                                                       'cki.exchange.datawarehouse.kcidb')
RABBITMQ_KEEPALIVE_S = misc.get_env_int('RABBITMQ_KEEPALIVE_S', 10)
RABBITMQ_CONFIGURED = bool(RABBITMQ_HOST)

#################################################################################
#                                                                               #
# Celery configuration                                                          #
#                                                                               #
#################################################################################

CELERY_BROKER_MODE = os.environ.get('CELERY_BROKER_MODE', 'filesystem')
if CELERY_BROKER_MODE == 'filesystem':
    # Filesystem mode
    # https://www.distributedpython.com/2018/07/03/simple-celery-setup/
    CELERY_FILESYSTEM_PATH = os.environ.get('CELERY_FILESYSTEM_PATH', '/tmp')
    CELERY_BROKER_URL = os.environ.get('CELERY_BROKER_URL', 'filesystem://')
    CELERY_BROKER_TRANSPORT_OPTIONS = {
        'data_folder_out': CELERY_FILESYSTEM_PATH,
        'data_folder_in': CELERY_FILESYSTEM_PATH,
        'store_processed': False,
    }
elif CELERY_BROKER_MODE == 'rabbitmq':
    if not RABBITMQ_CONFIGURED:
        raise Exception('Celery broker \'rabbitmq\' selected but RabbitMQ is not configured')

    RABBITMQ_CELERY_VHOST = os.environ.get('RABBITMQ_CELERY_VHOST', '/dw_celery')
    CELERY_BROKER_URL = [
        f'amqp://{RABBITMQ_USER}:{RABBITMQ_PASSWORD}@{host}:{RABBITMQ_PORT}/{RABBITMQ_CELERY_VHOST}'
        for host in RABBITMQ_HOST.split()
    ]
    CELERY_BROKER_USE_SSL = {
        'ca_certs': os.environ.get('REQUESTS_CA_BUNDLE'),
    }
else:
    raise Exception(f'Unsupported Celery broker mode: {CELERY_BROKER_MODE}')

#################################################################################
#                                                                               #
# Timers configuration                                                          #
#                                                                               #
#################################################################################

# Timer used to send the retriage notification after an IssueRegex was modified
TIMER_RETRIAGE_PERIOD_S = misc.get_env_int('TIMER_RETRIAGE_PERIOD_S', 60 * 5)

#################################################################################
#                                                                               #
# Email sending configuration                                                   #
#                                                                               #
#################################################################################

# SMTP server to use for sending emails
EMAIL_HOST = os.environ.get('EMAIL_HOST', 'localhost')
# Default sender for the emails
DEFAULT_FROM_EMAIL = os.environ.get('EMAIL_FROM', 'Datawarehouse <admin@datawarehouse>')
EMAIL_SEND_ENABLED = misc.get_env_bool('EMAIL_SEND_ENABLED', False)

#################################################################################
#                                                                               #
# Logging configuration                                                         #
#                                                                               #
#################################################################################

LOGS_PATH = os.environ.get('LOGS_PATH', '/tmp')
LOGGING = {
    'version': 1,
    'formatters': {
        'requests': {
            'format': '{asctime} - {name} {message}',
            'style': '{',
        },
        'cki': {
            'format': '{asctime} - [{levelname}] - {name} - {message}',
            'style': '{',
        }
    },
    'handlers': {
        'requests': {
            'level': 'INFO',
            'formatter': 'requests',
            'class': 'logging.FileHandler',
            'filename': f'{LOGS_PATH}/django-{gethostname()}-requests.log',
        },
        'cki': {
            'level': os.environ.get('CKI_LOGGING_LEVEL', 'INFO'),
            'class': 'logging.FileHandler',
            'filename': f'{LOGS_PATH}/django-{gethostname()}-cki.log',
            'formatter': 'cki'
        }
    },
    'loggers': {
        # Production server
        'django.request': {
            'handlers': ['requests'],
            'propagate': False,
        },
        # Development server
        'django.server': {
            'handlers': ['requests'],
            'propagate': False,
        },
        'cki': {
            'handlers': ['cki'],
            'propagate': False,
        },
        'djangosaml2': {
            'handlers': ['cki'],
            'propagate': False,
        },
    }
}

#################################################################################
#                                                                               #
# Feature flags and custom configurations                                       #
#                                                                               #
#################################################################################

# Enable signup
FF_SIGNUP_ENABLED = misc.get_env_bool('FF_SIGNUP_ENABLED', False)

# Amount of days before not confirmed accounts are deleted.
FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS = misc.get_env_int("FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS", 30)

CAPTCHA_NOISE_FUNCTIONS = ('captcha.helpers.noise_dots',)
CAPTCHA_FILTER_FUNCTIONS = ('captcha.helpers.post_smooth',)
CAPTCHA_CHALLENGE_FUNCT = 'captcha.helpers.random_char_challenge'
CAPTCHA_FOREGROUND_COLOR = '#000030'
CAPTCHA_TEST_MODE = not IS_PRODUCTION_OR_STAGING

# Send Issue Regression emails
FF_NOTIFY_ISSUE_REGRESSION = misc.get_env_bool('FF_NOTIFY_ISSUE_REGRESSION', False)

# Use avatar server for profile images
FF_EXTERNAL_AVATAR_ENABLED = misc.get_env_bool('FF_EXTERNAL_AVATAR_ENABLED', True)
EXTERNAL_AVATAR_URL = os.environ.get('EXTERNAL_AVATAR_URL', 'https://seccdn.libravatar.org/avatar')
# Default image for users with no avatar. Ej 'mp' (Mistery Person), etc
EXTERNAL_AVATAR_DEFAULT = os.environ.get('EXTERNAL_AVATAR_DEFAULT', 'identicon')
# Default user image to be used if avatar is disabled
DEFAULT_USER_IMAGE = 'img/default_user_image.png'

# URL to the Privacy Policy document. Displayed on the footer of all pages.
PRIVACY_POLICY_URL = os.environ.get('PRIVACY_POLICY_URL')

BEAKER_URL = os.environ.get('BEAKER_URL', '').rstrip('/')
GITLAB_URL = os.environ.get('GITLAB_URL', '').rstrip('/')

# TTL for authorization data stored in the user session
SESSION_AUTH_CACHE_TTL_S = misc.get_env_int('SESSION_AUTH_CACHE_TTL_S', 60 * 5)

# Default value for Artifact.valid_for field
ARTIFACTS_DEFAULT_VALID_FOR_DAYS = misc.get_env_int('ARTIFACTS_DEFAULT_VALID_FOR_DAYS', 60)
# Dictionary of: {bucket: valid_for_days} for known valid_for values
ARTIFACTS_KNOWN_VALID_FOR = yaml.safe_load(os.environ.get('ARTIFACTS_KNOWN_VALID_FOR', '{}'))

#################################################################################
#                                                                               #
# SAML2 Support configuration                                                   #
#                                                                               #
#################################################################################

FF_SAML_ENABLED = misc.get_env_bool('FF_SAML_ENABLED')
if FF_SAML_ENABLED:
    INSTALLED_APPS.append(
        'djangosaml2'
    )
    MIDDLEWARE.append(
        'djangosaml2.middleware.SamlSessionMiddleware'
    )
    AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
        'datawarehouse.saml.Saml2Backend',
    )

    SESSION_EXPIRE_AT_BROWSER_CLOSE = True

    SAML_ALLOWED_HOSTS = []
    SAML_ATTRIBUTE_MAPPING = {
        'email': ('email',),
        'uid': ('username',),
        'cn': ('first_name',),
    }
    SAML_CREATE_UNKNOWN_USER = True
    SAML_DEFAULT_BINDING = saml2.BINDING_HTTP_POST
    SAML_DJANGO_USER_MAIN_ATTRIBUTE = 'email'
    SAML_DJANGO_USER_MAIN_ATTRIBUTE_LOOKUP = '__iexact'
    SAML_IGNORE_LOGOUT_ERRORS = True
    SAML_LOGOUT_REQUEST_PREFERRED_BINDING = saml2.BINDING_HTTP_POST

    SAML_LOADED_CONFIG = yaml.safe_load(os.environ['SAML_CONFIG'])
    SAML_NAME = SAML_LOADED_CONFIG['name']
    SAML_CONFIG = {
        'xmlsec_binary': '/usr/bin/xmlsec1',
        'entityid': DATAWAREHOUSE_DOMAIN_NAME,
        'attribute_map_dir': SAML_LOADED_CONFIG['attribute_map_dir'],
        'service': {
            'sp': {
                'name': DATAWAREHOUSE_DOMAIN_NAME,
                'name_id_format': saml2.saml.NAMEID_FORMAT_PERSISTENT,
                'endpoints': {
                    'assertion_consumer_service': [
                        (DATAWAREHOUSE_URL + '/saml2/acs/', saml2.BINDING_HTTP_POST),
                    ],
                    'single_logout_service': [
                        (DATAWAREHOUSE_URL + '/saml2/ls/', saml2.BINDING_HTTP_REDIRECT),
                        (DATAWAREHOUSE_URL + '/saml2/ls/post', saml2.BINDING_HTTP_POST),
                    ],
                },
                'force_authn': False,
                'name_id_format_allow_create': False,
                'required_attributes': ['uid', 'email', 'cn'],
                'allow_unsolicited': True,
                'allow_unknown_attributes': True,
                'only_use_keys_in_metadata': True,
            },
        },
        'debug': DEBUG,
        'metadata': SAML_LOADED_CONFIG['metadata'],
        'key_file': SAML_LOADED_CONFIG['signing']['key_path'],
        'cert_file': SAML_LOADED_CONFIG['signing']['cert_path'],
        'encryption_keypairs': [{
            'key_file': SAML_LOADED_CONFIG['encryption']['key_path'],
            'cert_file': SAML_LOADED_CONFIG['encryption']['cert_path'],
        }],
        'contact_person': SAML_LOADED_CONFIG['contact_person'],
        'organization': SAML_LOADED_CONFIG['organization'],
    }

#################################################################################
#                                                                               #
# Cache support configuration                                                   #
#                                                                               #
#################################################################################

FF_CACHE_ANONYMOUS = misc.get_env_bool('FF_CACHE_ANONYMOUS', False)
DEFAULT_CACHE_TTL_S = misc.get_env_int('DEFAULT_CACHE_TTL_S', 60 * 5)
MEMCACHED_SERVER = os.environ.get('MEMCACHED_SERVER')
if MEMCACHED_SERVER:
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.PyMemcacheCache',
            'LOCATION': MEMCACHED_SERVER,
        }
    }

#################################################################################
#                                                                               #
# LDAP groups sync support configuration                                        #
#                                                                               #
#################################################################################

FF_LDAP_GROUP_SYNC = misc.get_env_bool('FF_LDAP_GROUP_SYNC', False)
if FF_LDAP_GROUP_SYNC:
    LDAP_CONFIG = {
        'server_url': os.environ['LDAP_SERVER_URL'],
        'base_search': os.environ['LDAP_BASE_SEARCH'],
        'members_field': os.environ.get('LDAP_MEMBERS_FIELD', 'uniqueMember'),
        'email_field': os.environ.get('LDAP_EMAIL_FIELD', 'mail'),
    }
