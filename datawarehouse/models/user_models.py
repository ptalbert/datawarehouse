"""Models related to the users."""
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM

User = get_user_model()


class LDAPGroupLink(EMOM('ldap_group_mapping'), models.Model):
    """
    Model for LDAPGroupLink.

    Linked to a regular Django Auth group represents a query that
    synchronizes the group users to LDAP members.
    """

    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='saml_link')
    filter_query = models.TextField()
    extra_users = models.ManyToManyField(User, related_name='forced_groups', blank=True)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.group} - {self.filter_query}'


class SAMLUser(EMOM('saml_user'), models.Model):
    """
    Model for SAMLUser.

    Linked to a regular Django Auth user represents that the
    user is linked to a SAML2 account.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.user.username}'


class SubscriptionVisibility(models.TextChoices):
    """Visibility of user subscription."""

    CC = 'cc'
    BCC = 'bcc'


class UserSubscriptions(EMOM('user_subscription'), models.Model):
    """
    Model for UserSubscription.

    Linked to a regular Django Auth user represents the subscriptions
    a user signed up to.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='subscriptions')
    issue_regression_subscribed_at = models.DateTimeField(null=True, blank=True)
    issue_regression_visibility = models.CharField(max_length=3, choices=SubscriptionVisibility.choices)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.user.username}'

    @property
    def issue_regression(self):
        """Return True if the user is subscribed to issue regressions."""
        return bool(self.issue_regression_subscribed_at)


class UserPreferences(EMOM('user_preference'), models.Model):
    """
    Model for UserPreferences.

    Linked to a regular Django Auth user represents various user preferences.
    """

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='preferences')
    account_deletion_warning_sent_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.user.username}'
