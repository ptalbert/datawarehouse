"""API Views."""
from cki_lib.logger import get_logger
from rest_framework import generics

from datawarehouse import models
from datawarehouse import serializers
from datawarehouse import utils

LOGGER = get_logger(__name__)


class IssueGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single Issue."""

    serializer_class = serializers.IssueSerializer
    queryset = models.Issue.objects.all()
    lookup_fields = (
        ('issue_id', 'id'),
    )


class IssueList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get a list of Issues."""

    serializer_class = serializers.IssueSerializer
    queryset = models.Issue.objects.all()
    lookup_fields = (
        ('resolved', 'resolved_at__isnotnull'),
    )


class TestSingle(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Endpoint for handling single test."""

    serializer_class = serializers.TestSerializer
    queryset = models.Test.objects.all()
    lookup_fields = (
        ('test_id', 'id'),
    )


class TestList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Endpoint for handling many tests."""

    serializer_class = serializers.TestSerializer
    queryset = models.Test.objects.all()


class IssueRegexGet(utils.MultipleFieldLookupMixin, generics.RetrieveAPIView):
    """Get a single IssueRegex."""

    serializer_class = serializers.IssueRegexSerializer
    queryset = models.IssueRegex.objects.all()
    lookup_fields = (
        ('issue_regex_id', 'id'),
    )


class IssueRegexList(utils.MultipleFieldLookupMixin, generics.ListAPIView):
    """Get a list of IssueRegex."""

    serializer_class = serializers.IssueRegexSerializer
    queryset = models.IssueRegex.objects.all()
