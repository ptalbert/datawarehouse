# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Patch models file."""
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM

# Patches stuff
from datawarehouse.utils import parse_patches_from_urls

from .utils import Manager


class PatchManager(Manager):
    """Natural key for Test."""

    def get_by_natural_key(self, url, subject):
        """Lookup the object by the natural key."""
        return self.get(url=url, subject=subject)


class Patch(EMOM('patch'), models.Model):
    """Model for Patch."""

    url = models.CharField(max_length=500)
    subject = models.CharField(max_length=200)

    objects = PatchManager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.url}'

    def natural_key(self):
        """Return the natural key."""
        return (self.url, self.subject)

    class Meta:
        """Metadata."""

        ordering = ('id',)
        unique_together = ('url', 'subject')

    @property
    def gui_url(self):
        """Return a user-usable URL."""
        return self.url

    @classmethod
    def create_from_patchset_files(cls, patchset_files: list[dict[str, str]]) -> list['Patch']:
        """
        Create Patch instances from list of dicts containing the keys 'url' and optionally 'name'.

        Whenever "name" is missing, get the subject from the respective patch URL.

        Return a list of Patch instances, creating them if necessary.
        """
        # Fetch patches' subject when they have no name
        patches_without_name = [patch for patch in patchset_files if not patch.get('name', '')]
        subjects = parse_patches_from_urls(patch['url'] for patch in patches_without_name)
        for patch, subject in zip(patches_without_name, subjects):
            patch['name'] = subject

        return [
            cls.objects.get_or_create(
                url=patch['url'],
                subject=patch['name']
            )[0]
            for patch in patchset_files
        ]
