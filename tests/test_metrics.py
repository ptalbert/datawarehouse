"""Test metrics module."""
import datetime
from unittest import mock

from django.contrib.auth import get_user_model
from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import metrics
from datawarehouse import models
from tests import utils


class TestUpdateTimeToBuild(utils.TestCase):
    """Test update_time_to_build method."""
    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    @mock.patch('datawarehouse.metrics.TIME_TO_BUILD')
    def test_missing_fields(self, mock_time_to_build):
        """Test update_time_to_build method with incorrect values."""
        build = models.KCIDBBuild.objects.last()
        metrics.update_time_to_build(build)
        self.assertFalse(mock_time_to_build.called)

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.metrics.TIME_TO_BUILD')
    def test_ok(self, mock_time_to_build):
        """Test update_time_to_build method with correct values."""
        build = models.KCIDBBuild.objects.last()
        build.architecture = 2
        build.start_time = timezone.now()
        build.duration = 15
        build.save()
        build.checkout.start_time = timezone.now() - datetime.timedelta(minutes=10)
        build.checkout.save()

        metrics.update_time_to_build(build)

        mock_time_to_build.assert_has_calls([
            mock.call.labels('aarch64'),
            mock.call.labels().observe(615)
        ])


class TestUpdateTimeToReport(utils.TestCase):
    """Test update_time_to_report method."""
    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    @mock.patch('datawarehouse.metrics.TIME_TO_REPORT')
    def test_missing_fields(self, mock_time_to_report):
        """Test update_time_to_build method with incorrect values."""
        checkout = models.KCIDBCheckout.objects.last()
        metrics.update_time_to_report(checkout)
        self.assertFalse(mock_time_to_report.called)

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.metrics.TIME_TO_REPORT')
    def test_ok(self, mock_time_to_report):
        """Test update_time_to_build method with correct values."""
        checkout = models.KCIDBCheckout.objects.last()
        checkout.start_time = timezone.now() - datetime.timedelta(minutes=10)
        checkout.save()

        metrics.update_time_to_report(checkout)

        mock_time_to_report.assert_has_calls([
            mock.call.observe(600)
        ])


class TestUpdateUnfinishedBuilds(utils.TestCase):
    """Test update_unfinished_builds method."""
    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    @mock.patch('datawarehouse.metrics.UNFINISHED_BUILDS')
    def test_unfinished(self, mock_unfinished_builds):
        """Test update_unfinished_builds with unfinshed builds."""
        models.KCIDBBuild.objects.update(valid=None)

        metrics.update_unfinished_builds()

        mock_unfinished_builds.assert_has_calls([
            mock.call.set(models.KCIDBBuild.objects.count())
        ])

    @mock.patch('datawarehouse.metrics.UNFINISHED_BUILDS')
    def test_finished(self, mock_unfinished_builds):
        """Test update_unfinished_builds with unfinshed builds."""
        models.KCIDBBuild.objects.update(valid=True)
        metrics.update_unfinished_builds()
        mock_unfinished_builds.assert_has_calls([
            mock.call.set(0)
        ])

    @mock.patch('datawarehouse.metrics.UNFINISHED_BUILDS')
    def test_mixed(self, mock_unfinished_builds):
        """Test update_unfinished_builds with unfinshed builds."""
        models.KCIDBBuild.objects.update(valid=None)
        build = models.KCIDBBuild.objects.last()
        build.valid = False
        build.save()

        metrics.update_unfinished_builds()

        mock_unfinished_builds.assert_has_calls([
            mock.call.set(models.KCIDBBuild.objects.count() - 1)
        ])


class TestUpdateUnfinishedTests(utils.TestCase):
    """Test update_unfinished_tests method."""
    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    @mock.patch('datawarehouse.metrics.UNFINISHED_TESTS')
    def test_unfinished(self, mock_unfinished_tests):
        """Test update_unfinished_tests with unfinshed tests."""
        models.KCIDBTest.objects.update(status=None)

        metrics.update_unfinished_tests()

        mock_unfinished_tests.assert_has_calls([
            mock.call.set(models.KCIDBTest.objects.count())
        ])

    @mock.patch('datawarehouse.metrics.UNFINISHED_TESTS')
    def test_finished(self, mock_unfinished_tests):
        """Test update_unfinished_tests with unfinshed tests."""
        models.KCIDBTest.objects.update(status='F')
        metrics.update_unfinished_tests()
        mock_unfinished_tests.assert_has_calls([
            mock.call.set(0)
        ])

    @mock.patch('datawarehouse.metrics.UNFINISHED_TESTS')
    def test_mixed(self, mock_unfinished_tests):
        """Test update_unfinished_tests with unfinshed tests."""
        models.KCIDBTest.objects.update(status=None)
        test = models.KCIDBTest.objects.last()
        test.status = 'F'
        test.save()

        metrics.update_unfinished_tests()

        mock_unfinished_tests.assert_has_calls([
            mock.call.set(models.KCIDBTest.objects.count() - 1)
        ])


class TestUpdateIssues(utils.TestCase):
    """Test update_issues method."""
    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/fixtures/issue_regexes.yaml',
    ]

    def test_preconditions(self):
        """Ensure required conditions are OK for the tests."""
        self.assertTrue(models.Issue.objects.exists())
        self.assertTrue(models.Issue.objects.exists())

    @mock.patch('datawarehouse.metrics.ISSUES')
    def test_resolved_not_resolved(self, mock_issues):
        """Test update_issues correctly updates cki_issues_resolved."""
        models.Issue.objects.update(resolved_at=None)
        metrics.update_issues()
        mock_issues.assert_has_calls([
            mock.call.labels('true'),
            mock.call.labels().set(0),
            mock.call.labels('false'),
            mock.call.labels().set(models.Issue.objects.count()),
        ])

        mock_issues.reset_mock()

        models.Issue.objects.update(resolved_at=timezone.now())
        metrics.update_issues()
        mock_issues.assert_has_calls([
            mock.call.labels('true'),
            mock.call.labels().set(models.Issue.objects.count()),
            mock.call.labels('false'),
            mock.call.labels().set(0),
        ])

    @mock.patch('datawarehouse.metrics.ISSUE_REGEXES')
    def test_issue_regexes(self, mock_issue_regexes):
        """Test update_issues correctly updates cki_issue_regexes."""
        metrics.update_issues()
        mock_issue_regexes.set.assert_called_with(models.IssueRegex.objects.count())

    @mock.patch('datawarehouse.metrics.ISSUE_TAGGER')
    def test_issue_taggers(self, mock_issue_tagger):
        """Test update_issues correctly updates cki_issues_tagger."""
        test_user_1 = get_user_model().objects.create(username='test_1', email='test1@test.com')
        test_user_2 = get_user_model().objects.create(username='test_2', email='test2@test.com')
        issue = models.Issue.objects.last()
        models.IssueOccurrence.objects.create(issue=issue, created_by=test_user_1)
        models.IssueOccurrence.objects.create(issue=issue, created_by=test_user_1)
        models.IssueOccurrence.objects.create(issue=issue, created_by=test_user_2)

        metrics.update_issues()

        mock_issue_tagger.assert_has_calls([
            mock.call.labels(test_user_1.username),
            mock.call.labels().set(2),
            mock.call.labels(test_user_2.username),
            mock.call.labels().set(1),
        ])


class TestUpdateBaselines(utils.TestCase):
    """Test update_baselines method."""
    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
    ]

    @mock.patch('datawarehouse.metrics.BASELINES_CHECKOUTS')
    def test_baselines_checkouts(self, mock_baselines_checkouts):
        """Test BASELINES_CHECKOUTS values after update_baselines call."""
        origin = models.KCIDBOrigin.objects.get(name='redhat')
        checkouts = [
            # id, git_repository_url, git_repository_branch, valid, has_issue
            ('redhat:1', 'url_1', 'branch_1', True, True),
            ('redhat:2', 'url_2', 'branch_2', True, False),
            ('redhat:3', 'url_2', 'branch_2', True, False),  # Duplicated, should not affect the metrics
            ('redhat:4', 'url_3', 'branch_3', False, True),
            ('redhat:5', 'url_4', 'branch_4', False, False),
        ]

        for id, url, branch, valid, has_issue in checkouts:
            checkout = models.KCIDBCheckout.objects.create(
                origin=origin,
                id=id,
                git_repository_url=url,
                git_repository_branch=branch,
                tree=models.GitTree.objects.get_or_create(name=branch)[0],
                start_time=timezone.now(),
                valid=valid,
                scratch=False,
            )

            if has_issue:
                models.IssueOccurrence.objects.create(
                    issue=models.Issue.objects.first(),
                    kcidb_checkout=checkout
                )

        # Add extra non-baseline checkouts that should not affect the metrics
        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'non_baseline_{index}',
                    start_time=timezone.now(),
                    valid=True,
                    scratch=True,
                )
                for index in range(10)
            ]
        )

        metrics.update_baselines()

        mock_baselines_checkouts.assert_has_calls([
            mock.call.labels(valid=True, known_issues=True),
            mock.call.labels().set(1),
            mock.call.labels(valid=True, known_issues=False),
            mock.call.labels().set(1),
            mock.call.labels(valid=False, known_issues=True),
            mock.call.labels().set(1),
            mock.call.labels(valid=False, known_issues=False),
            mock.call.labels().set(1),
        ])

    @mock.patch('datawarehouse.metrics.BASELINES_BUILDS')
    def test_baselines_builds(self, mock_baselines_builds):
        """Test BASELINES_BUILDS values after update_baselines call."""
        origin = models.KCIDBOrigin.objects.get(name='redhat')
        builds = [
            # id, valid, has_issue
            ('redhat:1', True, True),
            ('redhat:2', True, False),
            ('redhat:3', True, False),
            ('redhat:4', False, True),
            ('redhat:5', False, False),

        ]
        checkout = models.KCIDBCheckout.objects.create(
            origin=origin,
            id='redhat:1',
            tree=models.GitTree.objects.get_or_create(name='branch')[0],
            start_time=timezone.now(),
            scratch=False,
        )

        for id, valid, has_issue in builds:
            build = models.KCIDBBuild.objects.create(
                checkout=checkout,
                origin=origin,
                id=id,
                architecture=2,
                valid=valid,
            )

            if has_issue:
                models.IssueOccurrence.objects.create(
                    issue=models.Issue.objects.first(),
                    kcidb_build=build
                )

        metrics.update_baselines()

        # Create list of calls for the metrics expected to be empty
        def labels_call(arch, valid, known_issues):
            return mock.call.labels(
                architecture=models.ArchitectureEnum(arch).name,
                valid=valid,
                known_issues=known_issues
            )

        def value_call(*_):
            return mock.call.labels().set(0)

        empty_metrics = [
            f(arch, valid, known_issues)
            for arch in set(models.ArchitectureEnum.values) - {2}  # 2: aarch64 has metrics
            for valid in (True, False)
            for known_issues in (True, False)
            for f in (labels_call, value_call)
        ]

        mock_baselines_builds.assert_has_calls([
            mock.call.labels(architecture='aarch64', valid=True, known_issues=True),
            mock.call.labels().set(1),
            mock.call.labels(architecture='aarch64', valid=True, known_issues=False),
            mock.call.labels().set(2),
            mock.call.labels(architecture='aarch64', valid=False, known_issues=True),
            mock.call.labels().set(1),
            mock.call.labels(architecture='aarch64', valid=False, known_issues=False),
            mock.call.labels().set(1),
        ] + empty_metrics)

    @mock.patch('datawarehouse.metrics.BASELINES_TESTS')
    def test_baselines_tests(self, mock_baselines_tests):
        """Test BASELINES_TESTS values after update_baselines call."""
        origin = models.KCIDBOrigin.objects.get(name='redhat')
        tests = [
            # id, status, has_issue
            ('redhat:1', 'P', True),
            ('redhat:2', 'P', False),
            ('redhat:3', 'P', False),
            ('redhat:4', 'F', True),
            ('redhat:5', 'F', False),

        ]
        checkout = models.KCIDBCheckout.objects.create(
            origin=origin,
            id='redhat:1',
            tree=models.GitTree.objects.get_or_create(name='branch')[0],
            start_time=timezone.now(),
            scratch=False,
        )
        build = models.KCIDBBuild.objects.create(
            checkout=checkout,
            origin=origin,
            id='redhat:1',
            architecture=2,
        )

        for id, status, has_issue in tests:
            test = models.KCIDBTest.objects.create(
                build=build,
                origin=origin,
                id=id,
                status=status,
                waived=False,
            )

            if has_issue:
                models.IssueOccurrence.objects.create(
                    issue=models.Issue.objects.first(),
                    kcidb_test=test
                )

        metrics.update_baselines()

        # Create list of calls for the metrics expected to be empty
        def labels_call(arch, status, known_issues):
            return mock.call.labels(
                architecture=models.ArchitectureEnum(arch).name,
                status=status,
                known_issues=known_issues
            )

        def value_call(*_):
            return mock.call.labels().set(0)

        empty_metrics = [
            f(arch, status, known_issues)
            for arch in set(models.ArchitectureEnum.values) - {2}  # 2: aarch64 has metrics
            for status in models.ResultEnum.names
            for known_issues in (True, False)
            for f in (labels_call, value_call)
        ]

        mock_baselines_tests.assert_has_calls([
            mock.call.labels(architecture='aarch64', status='FAIL', known_issues=True),
            mock.call.labels().set(1),
            mock.call.labels(architecture='aarch64', status='FAIL', known_issues=False),
            mock.call.labels().set(1),
            mock.call.labels(architecture='aarch64', status='ERROR', known_issues=True),
            mock.call.labels().set(0),
            mock.call.labels(architecture='aarch64', status='ERROR', known_issues=False),
            mock.call.labels().set(0),
            mock.call.labels(architecture='aarch64', status='PASS', known_issues=True),
            mock.call.labels().set(1),
            mock.call.labels(architecture='aarch64', status='PASS', known_issues=False),
            mock.call.labels().set(2),
            mock.call.labels(architecture='aarch64', status='DONE', known_issues=True),
            mock.call.labels().set(0),
            mock.call.labels(architecture='aarch64', status='DONE', known_issues=False),
            mock.call.labels().set(0),
            mock.call.labels(architecture='aarch64', status='SKIP', known_issues=True),
            mock.call.labels().set(0),
            mock.call.labels(architecture='aarch64', status='SKIP', known_issues=False),
            mock.call.labels().set(0),
            mock.call.labels(architecture='aarch64', status='NEW', known_issues=True),
            mock.call.labels().set(0),
            mock.call.labels(architecture='aarch64', status='NEW', known_issues=False),
            mock.call.labels().set(0),
        ] + empty_metrics)


class TestUpdateCheckoutMetrics(utils.TestCase):
    """Test update_checkout_metrics method."""
    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    @mock.patch('datawarehouse.metrics.TESTS_STATUS')
    @mock.patch('datawarehouse.metrics.BUILDS_VALID')
    @mock.patch('datawarehouse.metrics.CHECKOUTS_VALID')
    def test_ok(self, mock_checkouts, mock_builds, mock_tests):
        """Test update_checkout_metrics method."""
        models.KCIDBBuild.objects.update(architecture=models.ArchitectureEnum.aarch64)
        checkout = models.KCIDBCheckout.objects.last()

        metrics.update_checkout_metrics(checkout)

        mock_checkouts.assert_has_calls([
            mock.call.labels(valid=None, known_issues=False),
            mock.call.labels().inc(),
        ])

        mock_builds.assert_has_calls([
            mock.call.labels(valid=True, known_issues=False, architecture='aarch64'),
            mock.call.labels().inc(),
            mock.call.labels(valid=False, known_issues=False, architecture='aarch64'),
            mock.call.labels().inc(),
            mock.call.labels(valid=None, known_issues=False, architecture='aarch64'),
            mock.call.labels().inc(),
        ])

        mock_tests.assert_has_calls([
            mock.call.labels(status=None, known_issues=False, architecture='aarch64', waived=False),
            mock.call.labels().inc(),
            mock.call.labels(status='FAIL', known_issues=False, architecture='aarch64', waived=False),
            mock.call.labels().inc(),
            mock.call.labels(status='PASS', known_issues=False, architecture='aarch64', waived=False),
            mock.call.labels().inc(),
        ])
