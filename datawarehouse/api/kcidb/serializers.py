# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# pylint: disable=no-self-use
"""Serializers."""
from cki_lib.logger import get_logger
from rest_framework import serializers

from datawarehouse import models
from datawarehouse.serializers import TestMaintainerSerializer

VERSION = {'major': 4, 'minor': 0}
LOGGER = get_logger(__name__)


class NoEmptyFieldSerializer(serializers.ModelSerializer):
    """Don't serialize empty fields."""

    @staticmethod
    def _value_is_empty(value):
        """Return True if the value is empty."""
        return (
            value is None or
            value == {} or
            value == []
        )

    def to_representation(self, instance):
        """Filter out empty values from the representation."""
        result = super().to_representation(instance)
        return self.clean_data(result)

    def clean_data(self, data):
        """Remove empty fields from data."""
        return {
            key: value for key, value in data.items()
            if not self._value_is_empty(value)
        }


class MaintainerSerializer(serializers.ModelSerializer):
    """Serializer for Maintainer model."""

    class Meta:
        """Metadata."""

        model = models.Maintainer
        fields = ('name', 'email')


class KCIDBPatchMBOXSerializer(serializers.ModelSerializer):
    """Serializer for Patch KCIDB styled."""

    name = serializers.CharField(source='subject', read_only=True)

    class Meta:
        """Metadata."""

        model = models.Patch
        fields = ('name', 'url')


class KCIDBFileSerializer(serializers.ModelSerializer):
    """Serializer for File KCIDB styled."""

    class Meta:
        """Metadata."""

        model = models.Artifact
        fields = ('url', 'name')


class ProvenanceComponent(NoEmptyFieldSerializer):
    """Serializer for ProvenanceComponent."""

    function = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.ProvenanceComponent
        fields = ('url', 'function', 'environment', 'service_name')

    def get_function(self, obj):
        """Return function."""
        return obj.get_function_display()


class KCIDBCheckoutSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBCheckout."""

    origin = serializers.CharField(source='origin.name')
    tree_name = serializers.CharField(source='tree.name', read_only=True)
    patchset_files = KCIDBPatchMBOXSerializer(many=True, source='patches')
    log_url = serializers.CharField(source='log.url', read_only=True)
    contacts = serializers.SerializerMethodField()
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBCheckout
        fields = ('id', 'origin', 'tree_name',
                  'git_repository_url', 'git_repository_branch',
                  'git_commit_hash', 'git_commit_name',
                  'patchset_files', 'patchset_hash', 'message_id', 'comment',
                  'start_time', 'log_url', 'log_excerpt',
                  'contacts', 'valid', 'misc',
                  )

    def get_contacts(self, checkout):
        """Return contacts as a flat list of emails."""
        return list(checkout.contacts.values_list('email', flat=True))

    def get_misc(self, checkout):
        """Return misc field."""
        misc = {
            'kcidb': {'version': VERSION},
            'iid': checkout.iid,
            'is_public': checkout.is_public,
            'is_missing_triage': checkout.is_missing_triage,
            'actions': {
                'last_triaged_at': serializers.DateTimeField().to_representation(checkout.last_triaged_at),
            },
            'brew_task_id': checkout.brew_task_id,
            'submitter': checkout.submitter.email if checkout.submitter else None,
            'scratch': checkout.scratch,
            'retrigger': checkout.retrigger,
            'patchset_modified_files': checkout.patchset_modified_files,
            'related_merge_request': checkout.related_merge_request,
            'send_ready_for_test_pre': checkout.notification_send_ready_for_test_pre,
            'send_ready_for_test_post': checkout.notification_send_ready_for_test_post,
            'kernel_version': checkout.kernel_version,
            'source_package_name': checkout.source_package_name,
            'provenance': ProvenanceComponent(checkout.provenance.all(), many=True).data,
        }
        return self.clean_data(misc)


class KCIDBBuildSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBBuild."""

    checkout_id = serializers.CharField(source='checkout.id')
    origin = serializers.CharField(source='origin.name')
    architecture = serializers.SerializerMethodField()
    log_url = serializers.CharField(source='log.url', read_only=True)
    compiler = serializers.CharField(source='compiler.name', read_only=True)
    input_files = KCIDBFileSerializer(many=True)
    output_files = KCIDBFileSerializer(many=True)
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBBuild
        fields = ('checkout_id', 'id', 'origin', 'comment',
                  'start_time', 'duration', 'architecture',
                  'command', 'compiler', 'input_files', 'output_files', 'config_name',
                  'config_url', 'log_url', 'log_excerpt', 'valid', 'misc',
                  )

    def get_architecture(self, obj):
        """Return architecture name."""
        return obj.get_architecture_display()

    def get_revision_id(self, build):
        """
        Return compatibility revision_id.

        This is necessary for KCIDB Upstream Forwarder service, and
        should be removed once upstream handles v4 schema.
        """
        revision_id = build.checkout.git_commit_hash
        if build.checkout.patchset_hash:
            revision_id += '+' + build.checkout.patchset_hash

        return revision_id

    def get_misc(self, build):
        """Return misc field."""
        misc = {
            'kcidb': {'version': VERSION},
            'iid': build.iid,
            'is_public': build.is_public,
            'is_missing_triage': build.is_missing_triage,
            'revision_id': self.get_revision_id(build),
            'actions': {
                'last_triaged_at': serializers.DateTimeField().to_representation(build.last_triaged_at),
            },
            'debug': build.debug,
            'kpet_tree_name': build.kpet_tree_name,
            'kpet_tree_family': build.kpet_tree_family,
            'package_name': build.package_name,
            'testing_skipped_reason': build.testing_skipped_reason,
            'retrigger': build.retrigger,
            'provenance': ProvenanceComponent(build.provenance.all(), many=True).data,
        }

        return self.clean_data(misc)


class EnvironmentSerializer(serializers.ModelSerializer):
    """Serializer for Environment (BeakerResource)."""

    comment = serializers.CharField(source='fqdn')

    class Meta:
        """Metadata."""

        model = models.BeakerResource
        fields = ('comment', )


class KCIDBTestSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBTest."""

    build_id = serializers.CharField(source='build.id')
    origin = serializers.CharField(source='origin.name')
    environment = EnvironmentSerializer()
    comment = serializers.CharField(source='test.name', read_only=True)
    path = serializers.CharField(source='test.universal_id', read_only=True)
    status = serializers.SerializerMethodField()
    output_files = KCIDBFileSerializer(many=True)
    misc = serializers.SerializerMethodField()
    log_url = serializers.CharField(source='log.url', read_only=True)

    class Meta:
        """Metadata."""

        model = models.KCIDBTest
        fields = ('build_id', 'id', 'origin', 'environment',
                  'path', 'comment', 'status', 'waived',
                  'start_time', 'duration', 'output_files', 'misc',
                  'log_url', 'log_excerpt',
                  )

    def get_status(self, obj):
        """Return status name."""
        return obj.get_status_display()

    def get_misc(self, test):
        """Return misc field."""
        misc = {
            'kcidb': {'version': VERSION},
            'iid': test.iid,
            'is_public': test.is_public,
            'is_missing_triage': test.is_missing_triage,
            'actions': {
                'last_triaged_at': serializers.DateTimeField().to_representation(test.last_triaged_at),
            },
            'retrigger': test.retrigger,
            'results': KCIDBTestResultSerializer(test.kcidbtestresult_set.all(), many=True).data,
            'provenance': ProvenanceComponent(test.provenance.all(), many=True).data,
        }

        if test.test:
            misc['maintainers'] = TestMaintainerSerializer(test.test.maintainers.all(), many=True).data

        return self.clean_data(misc)


class KCIDBTestResultSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBTestResult."""

    build_id = serializers.CharField(source='test.build_id')
    status = serializers.SerializerMethodField()
    output_files = KCIDBFileSerializer(many=True)
    misc = serializers.SerializerMethodField()
    comment = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBTestResult
        fields = ('id', 'name', 'status', 'output_files', 'misc', 'comment', 'build_id')

    def get_status(self, obj):
        """Return status name."""
        return obj.get_status_display()

    def get_misc(self, obj):
        """Return misc field."""
        return {
            'iid': obj.iid,
        }

    def get_comment(self, obj: models.KCIDBTestResult):
        """Return a human readable description of the KCIDBTestResult."""
        return f"{obj.test.test.name}/{obj.name}"


class KCIDBTestLightSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBTest with only lightweight keys."""

    build_id = serializers.CharField(source='build.id')
    comment = serializers.CharField(source='test.name', read_only=True)
    path = serializers.CharField(source='test.universal_id', read_only=True)
    status = serializers.SerializerMethodField()
    misc = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBTest
        fields = ('build_id', 'id', 'path', 'comment', 'status', 'waived',
                  'start_time', 'duration', 'log_excerpt', 'misc')

    def get_status(self, test):
        """Return status name."""
        return test.get_status_display()

    def get_misc(self, test):
        """Return misc field."""
        return {
            'iid': test.iid,
            'is_missing_triage': test.is_missing_triage,
        }


class KCIDBTestResultLightSerializer(NoEmptyFieldSerializer):
    """Serializer for KCIDBTestResult with only lightweight keys."""

    test_id = serializers.CharField(source='test.id')
    status = serializers.SerializerMethodField()
    misc = serializers.SerializerMethodField()
    comment = serializers.SerializerMethodField()

    class Meta:
        """Metadata."""

        model = models.KCIDBTestResult
        fields = ('id', 'name', 'status', 'misc', 'comment', 'test_id')

    def get_status(self, obj):
        """Return status name."""
        return obj.get_status_display()

    def get_misc(self, obj):
        """Return misc field."""
        return {
            'iid': obj.iid,
        }

    def get_comment(self, obj: models.KCIDBTestResult):
        """Return a human readable description of the KCIDBTestResult."""
        return f"{obj.test.test.name}/{obj.name}"
