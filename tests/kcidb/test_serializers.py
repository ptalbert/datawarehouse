"""Test kcidb serializers."""
import kcidb_io

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from tests import utils


class TestSerializers(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Test KCIDB-schemed serializers."""

    schema = kcidb_io.schema.v4.VERSION
    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_complete.yaml'
    ]

    def test_checkout(self):
        """Test KCIDBCheckout object serialization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'checkouts': [
                serializers.KCIDBCheckoutSerializer(
                    models.KCIDBCheckout.objects.get(iid=1)
                ).data
            ]
        }

        self.schema.validate_exactly(data)

    def test_build(self):
        """Test KCIDBBuild object serialization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'builds': [
                serializers.KCIDBBuildSerializer(
                    models.KCIDBBuild.objects.get(iid=1)
                ).data
            ]
        }

        self.schema.validate_exactly(data)

    def test_build_misc_revision_id(self):
        """Test compatibility revision_id."""
        build = serializers.KCIDBBuildSerializer(
            models.KCIDBBuild.objects.get(iid=1)
        ).data
        self.assertEqual(
            ('403cbf29a4e277ad4872515ec3854b175960bbdf'
             '+a18c010ffcf8852321ceee0820c766974567e3eabf348afefccfa2fec2b64e2b'),
            build['misc']['revision_id']
        )

    def test_test(self):
        """Test KCIDBTest object serialization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'tests': [
                serializers.KCIDBTestSerializer(
                    models.KCIDBTest.objects.get(iid=1)
                ).data
            ]
        }

        self.schema.validate_exactly(data)

    def test_test_data(self):
        """Test KCIDBTest object serialization returns the expected values."""
        data = serializers.KCIDBTestSerializer(
            models.KCIDBTest.objects.get(iid=1)
        ).data

        self.assertEqual(
            data,
            {
                'id': 'redhat:111218982',
                'build_id': 'redhat:887318',
                'origin': 'redhat',
                'environment': {'comment': 'hostname.redhat.com'},
                'path': 'boot',
                'comment': 'Boot Test',
                'waived': False,
                'start_time': '2020-06-03T15:52:25Z',
                'duration': 158,
                'output_files': [{'url': 'http://log.server/output.3', 'name': 'output.3'}],
                'misc': {
                    'kcidb': {'version': {'major': 4, 'minor': 0}},
                    'iid': 1,
                    'is_public': False,
                    'is_missing_triage': False,
                    'actions': {'last_triaged_at': None},
                    # This list depends on the KCIDBTestResultSerializer
                    'results': [
                        {
                            'id': 'redhat:111218982.1',
                            'build_id': '1',
                            'name': 'First result',
                            'status': 'PASS',
                            'comment': 'Boot Test/First result',
                            'misc': {'iid': 1},
                            'output_files': [{'url': 'http://log.server/output.2', 'name': 'output.2'}],
                        },
                    ]
                }
            }
        )

    def test_testresult_data(self):
        """Test KCIDBTest object serialization returns the expected values."""
        data = serializers.KCIDBTestResultSerializer(
            models.KCIDBTestResult.objects.get(iid=1)
        ).data

        self.assertEqual(
            data,
            {
                'id': 'redhat:111218982.1',
                'build_id': '1',
                'name': 'First result',
                'status': 'PASS',
                'comment': 'Boot Test/First result',
                'misc': {'iid': 1},
                'output_files': [
                    {'url': 'http://log.server/output.2', 'name': 'output.2'},
                ],
            },
        )
